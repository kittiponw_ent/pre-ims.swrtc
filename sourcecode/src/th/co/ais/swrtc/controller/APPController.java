package th.co.ais.swrtc.controller;

import java.util.ArrayList;

import javax.xml.bind.JAXBException;

import th.co.ais.swrtc.instances.APPInstance;
import th.co.ais.swrtc.instances.EC02Instance;
import th.co.ais.swrtc.interfaces.MessageType;
import th.co.ais.swrtc.interfaces.RetNumber;
import th.co.ais.swrtc.jaxb.InstanceContext;
import th.co.ais.swrtc.utils.ConfigureTool;
import th.co.ais.swrtc.utils.InstanceHandler;
import ec02.af.abstracts.AbstractAF;
import ec02.af.data.ECDialogue;
import ec02.af.data.EquinoxProperties;
import ec02.af.data.EquinoxRawData;
import ec02.af.exception.ActionProcessException;
import ec02.af.exception.ComposeInstanceException;
import ec02.af.exception.ConstructRawDataException;
import ec02.af.exception.ExtractInstanceException;
import ec02.af.exception.ExtractRawDataException;
import ec02.interfaces.IEC02;

public class APPController extends AbstractAF implements IEC02 {

	@SuppressWarnings("deprecation")
	@Override
	public ECDialogue actionProcess(EquinoxProperties properties,
			ArrayList<EquinoxRawData> rawDatas, Object instance)
			throws ActionProcessException {
		
		EC02Instance ec02Instance = (EC02Instance) instance;
		ec02Instance.setProperties(properties);
		ec02Instance.setAbstractAF( this );
		
		String currentState = properties.getState();
		
		// Timeout Handler
		if ( RetNumber.TIMEOUT.equals( properties.getRet() ) || rawDatas.size() == 0 ) {
			
			rawDatas.clear();
			
			EquinoxRawData r = new EquinoxRawData();
			r.setRet( RetNumber.TIMEOUT );
			r.setType( MessageType.RESPONSE );
			
			rawDatas.add( r );
			
		}
		
		StateManager stateManager = new StateManager( currentState, ec02Instance.getAppInstance().getSubState() );
		
		String nextState = stateManager.doAction( this, ec02Instance, rawDatas );
		
		properties.setState( nextState );
		properties.setRet( ec02Instance.getRet() );
		properties.setTimeout( ec02Instance.getTimeout() );
		properties.setDiag( "" );
		
		return new ECDialogue( properties, instance );
	}

	@Override
	public boolean verifyAFConfiguration(String afConfig) {
		
		ConfigureTool.initConfigureTool( this.getUtils().getHmWarmConfig() );
		
		try {
			InstanceContext.initMultimediaAuthContext();
			InstanceContext.initServerAssignmentContext();
			InstanceContext.initLdapQueryMessageContext();
			
		} catch (JAXBException e) {
			System.out.println( "Error Here" );
			e.printStackTrace();
			
			return false;
		}
		
		return true;
	}

	@Override
	public String composeInstance(Object instance) throws ComposeInstanceException {
		
		EC02Instance ec02Instance = (EC02Instance) instance;
		APPInstance appInstance = ec02Instance.getAppInstance();
		
		String encoded = InstanceHandler.encode(appInstance);
		
		return encoded;
		
	}

	@Override
	public ArrayList<EquinoxRawData> constructRawData(Object instance)
			throws ConstructRawDataException {
		
		EC02Instance ec02Instance = (EC02Instance) instance;
		
		return ec02Instance.getEquinoxRawDatas();
		
	}

	@Override
	public Object extractInstance(String instance) throws ExtractInstanceException {
		
		EC02Instance ec02Instance = new EC02Instance();
		APPInstance appInstance = null;
		
		appInstance = ( instance != null && !instance.isEmpty() ) 
				? InstanceHandler.decode(instance)
				: new  APPInstance();
		
		ec02Instance.setAppInstance( appInstance );

		return ec02Instance;
		
	}

	@Override
	public void extractRawData(Object instance, ArrayList<EquinoxRawData> rawDatas) 
			throws ExtractRawDataException {
		
		EC02Instance ec02Instance = (EC02Instance) instance;
		APPInstance appInstance = ec02Instance.getAppInstance();

		/* DON'T FORGET TO FIX THESE CODE */
		
		if ( ( appInstance.getOrigin() == null ) && ( rawDatas.size() > 0 ) ) {
			
			EquinoxRawData r = rawDatas.get( 0 );
			
			if ( RetNumber.NORMAL.equals( r.getRet() ) ) {
				appInstance.setOrigin( r.getOrig() );
				appInstance.setOriginInvoke( r.getInvoke() );
				appInstance.addIncomingInvocationNumber( r.getInvoke() );
				
			}
			
		}
	}

}
