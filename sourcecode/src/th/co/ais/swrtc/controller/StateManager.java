package th.co.ais.swrtc.controller;

import th.co.ais.swrtc.interfaces.States;
import th.co.ais.swrtc.interfaces.SubStates;
import th.co.ais.swrtc.states.ACTIVE;
import th.co.ais.swrtc.states.ESTABLISHED;
import th.co.ais.swrtc.states.IDLE;
import th.co.ais.swrtc.states.INITIATE_COMMUNICATION;
import th.co.ais.swrtc.states.INITIATE_INVITE;
import th.co.ais.swrtc.states.INITIATE_TERMINATION;
import th.co.ais.swrtc.states.S_W_PRACK_200;
import th.co.ais.swrtc.states.S_W_UPDATE;
import th.co.ais.swrtc.states.S_W_UPDATE_200;
import th.co.ais.swrtc.states.W_2ND_REGISTER;
import th.co.ais.swrtc.states.W_ANSWER;
import th.co.ais.swrtc.states.W_MAA;
import th.co.ais.swrtc.states.W_MNPDB;
import th.co.ais.swrtc.states.W_RINGING;
import th.co.ais.swrtc.states.W_SAA;
import ec02.af.abstracts.AbstractAFStateManager;

public class StateManager extends AbstractAFStateManager {

	public StateManager (String currentState, String subState ) {
		
		if( States.IDLE.equals( currentState )){
			this.afState = new IDLE();
			
		}
		else if ( States.WAIT_MULTIMEDIA_AUTH_ANSWER.equals( currentState ) ) {
			this.afState = new W_MAA();
			
		}
		else if ( States.WAIT_SECOND_REGISTER.equals( currentState ) ) {
			this.afState = new W_2ND_REGISTER();
			
		}
		else if ( States.WAIT_SERVER_ASSIGNMENT_ANSWER.equals( currentState ) ) {
			this.afState = new W_SAA();
			
		}
		else if ( States.ACTIVE.equals( currentState ) ) {
			this.afState = new ACTIVE();
			
		}
		else if ( States.WAIT_NETWORK_OPERATOR.equals( currentState ) ) {
			this.afState = new W_MNPDB();
			
		}
		else if ( States.INITIATE_INVITE.equals( currentState ) ) {
			this.afState = new INITIATE_INVITE();
			
		}
		else if ( States.WAIT_RINGING.equals( currentState ) ) {
			
			if ( SubStates.WAIT_PRACK_200.equals( subState ) ) {
				this.afState = new S_W_PRACK_200();
						
			}
			else if ( SubStates.WAIT_UPDATE.equals( subState ) ) {
				this.afState = new S_W_UPDATE();
				
			}
			else if ( SubStates.WAIT_UPDATE_200.equals( subState ) ) {
				this.afState = new S_W_UPDATE_200();
				
			}
			else {
				this.afState = new W_RINGING();
				
			}
			
		}
		else if ( States.WAIT_ANSWER.equals( currentState ) ) {
			
			if ( SubStates.WAIT_PRACK_200.equals( subState ) ) {
				this.afState = new S_W_PRACK_200();
						
			}
			else {
				this.afState = new W_ANSWER();
				
			}
			
		}
		else if ( States.INITIATE_COMMUNICATION.equals( currentState ) ) {
			this.afState = new INITIATE_COMMUNICATION();
			
		}
		else if ( States.ESTABLISHED.equals( currentState ) ) {
			this.afState = new ESTABLISHED();
			
		}
		else if ( States.INITIATE_TERMINATION.equals( currentState ) ) {
			this.afState = new INITIATE_TERMINATION();
			
		}
		
	}
	
}
