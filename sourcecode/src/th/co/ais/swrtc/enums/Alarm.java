package th.co.ais.swrtc.enums;

import th.co.ais.swrtc.interfaces.Statistic;
import ec02.af.utils.AlarmCategory;
import ec02.af.utils.AlarmSeverity;
import ec02.af.utils.AlarmType;

public enum Alarm {
	
	ApplicationReceiveIncompleteRegister (
			Statistic.APP_RECEIVE_INCOMPLETE_REGISTER,
			AlarmSeverity.MAJOR, AlarmCategory.NETWORK, AlarmType.Normal
			),
	
	APP_RECEIVE_MAR_REQUEST_ERROR (
			Statistic.APP_RECEIVE_MAR_REQUEST_ERROR,
			AlarmSeverity.MAJOR, AlarmCategory.NETWORK, AlarmType.Normal
			),
	APP_RECEIVE_MAR_REQUEST_REJECT (
			Statistic.APP_RECEIVE_MAR_REQUEST_REJECT,
			AlarmSeverity.MAJOR, AlarmCategory.NETWORK, AlarmType.Normal
			),
	APP_RECEIVE_MAR_REQUEST_ABORT (
			Statistic.APP_RECEIVE_MAR_REQUEST_ABORT,
			AlarmSeverity.MAJOR, AlarmCategory.NETWORK, AlarmType.Normal
			),
	APP_RECEIVE_MAR_REQUEST_TIMEOUT (
			Statistic.APP_RECEIVE_MAR_REQUEST_TIMEOUT,
			AlarmSeverity.MAJOR, AlarmCategory.NETWORK, AlarmType.Normal
			)
	
	;
	
	private String name;
	private AlarmSeverity severity;
	private AlarmCategory category;
	private AlarmType type;

	private Alarm( String name, AlarmSeverity severity,
			AlarmCategory category, AlarmType type ) {
		this.name = name;
		this.severity = severity;
		this.category = category;
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public AlarmSeverity getSeverity() {
		return severity;
	}

	public AlarmCategory getCategory() {
		return category;
	}

	public AlarmType getType() {
		return type;
	}

}
