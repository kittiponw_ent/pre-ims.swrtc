package th.co.ais.swrtc.enums;

import java.util.HashMap;

public enum EquinoxEvent {
	
	NORMAL	( "0", "Success" ),
	ERROR	( "1", "Error" ),
	REJECT	( "2", "Reject" ),
	ABORT	( "3", "Abort" ),
	TIMEOUT	( "4", "Timeout" )
	;

	private String code;
	private String desc;
	
	private EquinoxEvent( String code, String desc ) {
		this.code = code;
		this.desc = desc;
	}
	
	public String getCode() {
		return this.code;
	}
	
	public String getDescription() {
		return this.desc;
	}

	// Equinox Event Lookup -code-
	private static final HashMap<String, EquinoxEvent> lookup = new HashMap<String, EquinoxEvent>();
	static {
		for ( EquinoxEvent e : EquinoxEvent.values() ) {
			lookup.put( e.getCode(), e );
		}
	}
	
	public static EquinoxEvent getEquinoxEventFrom( String code ) {
		return lookup.get( code );
	}
	
}
