package th.co.ais.swrtc.enums;

import java.util.HashMap;

public enum SIPRequestMethod {
	
	INVITE		( "INVITE" ),
	ACK			( "ACK" ),
	BYE			( "BYE" ),
	CANCEL		( "CANCEL" ),
	PRACK		( "PRACK" ),
	REGISTER	( "REGISTER" ),
	UPDATE		( "UPDATE" )
	;

	private String method;
	
	private SIPRequestMethod ( String method ) {
		this.setMethod(method);
	}

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}
	
	// E01-Command Lookup
	private static final HashMap<String, SIPRequestMethod> lookup = new HashMap<String, SIPRequestMethod>();
	static {
		for ( SIPRequestMethod s : SIPRequestMethod.values() ) {
			lookup.put( s.getMethod(), s );
		}
	}
	
	public static SIPRequestMethod getSipRequestMethod ( String method ) {
		return lookup.get( method );
	}
	
}
