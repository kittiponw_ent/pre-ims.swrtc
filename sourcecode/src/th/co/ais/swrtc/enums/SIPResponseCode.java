package th.co.ais.swrtc.enums;

import java.util.HashMap;


public enum SIPResponseCode {
	
	TRYING					( "100", "Trying" ),
	RINGING					( "180", "Ringing" ),
	SESSION_IN_PROGRESS		( "183", "Session in Progress" ),
	
	OK						( "200", "OK" ),
	
	BAD_REQUEST				( "400", "Bad Request" ),
	UNAUTHORIZED			( "401", "Unauthorized" ),
	
	SERVER_INTERNAL_ERROR	( "500", "Server Internal Errror" ),
	SERVER_TIME_OUT			( "504", "Server Time-out" ) 
	
	;
	
	private String code;
	private String reason;
	
	private SIPResponseCode(String code, String reason) {
		this.setCode( code );
		this.setReason( reason );
	}
	
	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}


	private static final HashMap<String, SIPResponseCode> lookup = new HashMap<String, SIPResponseCode>();
	static {
		for ( SIPResponseCode e : SIPResponseCode.values() ) {
			lookup.put( e.getCode(), e );
		}
	}
	
	public static SIPResponseCode getResponseCode( String code ) {
		return lookup.get( code );
	}
}
