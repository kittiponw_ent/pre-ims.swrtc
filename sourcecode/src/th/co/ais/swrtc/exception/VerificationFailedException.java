package th.co.ais.swrtc.exception;

import th.co.ais.swrtc.enums.Alarm;
import th.co.ais.swrtc.enums.SIPResponseCode;


public class VerificationFailedException extends Exception {
	
	private SIPResponseCode sipResponseCode;
	private String statistic;
	private Alarm alarm;

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public VerificationFailedException() {}

	public SIPResponseCode getSIPResponseCode() {
		return sipResponseCode;
	}

	public void setSIPResponseCode( SIPResponseCode sipResponseCode ) {
		this.sipResponseCode = sipResponseCode;
	}

	public String getStatistic() {
		return statistic;
	}

	public void setStatistic(String statistic) {
		this.statistic = statistic;
	}

	public Alarm getAlarm() {
		return alarm;
	}

	public void setAlarm(Alarm alarm) {
		this.alarm = alarm;
	}

}
