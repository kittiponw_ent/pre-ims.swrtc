package th.co.ais.swrtc.instances;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import th.co.ais.swrtc.utils.AppTermination;

public class APPInstance {
	
	private String origin;
	private String originInvoke;
	
	private String incomingEquinoxSession;
	private String actionType;
	private String callId;
	
	private String applicationServer = "Test-Application-Server";
	
	private ArrayList<String> incomingInvocationNumbers;
	private ArrayList<String> pendingRequests;
	
	private String generatedNonce;
	private String subState;
	
	private String event;
	private String terminationMethod;
	
	private long lifeSpan;
	
	private long lastInstanceUpdatedTime;
	
	/*
	 * OUTGOING TIMEOUT USED ONLY 'BYE FROM AS' EVENT OCCUR
	 */
	private long outgoingTimeout;
	
	private SIPMessage sipMessage;
	
	private AppTermination appTermination;
	
	public APPInstance() {
		this.setIncomingInvocationNumbers( new ArrayList<String>() );
		this.setPendingRequests( new ArrayList<String>() );
		this.setAppTermination( new AppTermination() );
	}

	public AppTermination getAppTermination() {
		return appTermination;
	}

	public void setAppTermination(AppTermination appTermination) {
		this.appTermination = appTermination;
	}

	public long getLastInstanceUpdatedTime() {
		return lastInstanceUpdatedTime;
	}

	public void setLastInstanceUpdatedTime(long lastInstanceUpdatedTime) {
		this.lastInstanceUpdatedTime = lastInstanceUpdatedTime;
	}

	public SIPMessage getSipMessage() {
		return sipMessage;
	}

	public void setSIPMessage(SIPMessage sipMessage) {
		this.sipMessage = sipMessage;
	}
	
	public void clearSIPMessage() {
		this.sipMessage = null;
	}

	public String getGeneratedNonce() {
		return generatedNonce;
	}

	public void setGeneratedNonce(String generatedNonce) {
		this.generatedNonce = generatedNonce;
	}

	public String getSubState() {
		return subState;
	}

	public void setSubState(String subState) {
		this.subState = subState;
	}
	
	public void clearSubState() {
		this.setSubState( null );
	}

	public String getApplicationServer() {
		return applicationServer;
	}

	public void setApplicationServer(String applicationServer) {
		this.applicationServer = applicationServer;
	}

	public String getActionType() {
		return actionType;
	}

	public void setActionType(String actionType) {
		this.actionType = actionType;
	}

	// PENDING REQUEST
	public ArrayList<String> getPendingRequests() {
		return pendingRequests;
	}

	public void setPendingRequests (ArrayList<String> pendingTasks) {
		this.pendingRequests = pendingTasks;
	}
	
	public void addPendingRequest ( String invoke ) {
		this.pendingRequests.add( invoke );
	}
	
	public boolean containRequest ( String invoke ) {
		return this.pendingRequests.contains( invoke );
	}
	
	public void completePendingRequest ( String invoke ) {
		if ( this.containRequest( invoke ) ) {
			this.pendingRequests.remove(
					this.pendingRequests.indexOf( invoke )
					);
		}
		
	}

	public String getEvent() {
		return event;
	}

	public void setEvent(String event) {
		this.event = event;
	}
	
	public void clearEvent() {
		this.setEvent( null );
	}

	// CALL TERMINATION METHOD
	public String getCallTerminationMethod() {
		return terminationMethod;
	}

	public void setCallTerminationMethod(String terminationMethod) {
		this.terminationMethod = terminationMethod;
	}
	
	public void clearCallTerminationMethod() {
		this.setCallTerminationMethod( null );
	}

	public String getIncomingEquinoxSession() {
		return incomingEquinoxSession;
	}

	public void setIncomingEquinoxSession(String incomingEquinoxSession) {
		this.incomingEquinoxSession = incomingEquinoxSession;
	}

	public String getCallId() {
		return callId;
	}

	public void setCallId(String callId) {
		this.callId = callId;
	}

	// INCOMING INVOKE
	public ArrayList<String> getIncomingInvocationNumbers() {
		return incomingInvocationNumbers;
	}

	public void setIncomingInvocationNumbers(ArrayList<String> incomingInvocationNumber) {
		this.incomingInvocationNumbers = incomingInvocationNumber;
	}
	
	public void addIncomingInvocationNumber ( String initialInvoke ) {
		this.incomingInvocationNumbers.add( initialInvoke );
	}
	
	// ORIGIN
	public String getOrigin() {
		return origin;
	}

	public void setOrigin(String origin) {
		this.origin = origin;
	}
	
	public void clearOrigin() {
		this.origin = null;
	}

	// ORIGIN INVOCATION NUMBER
	public String getOriginInvoke() {
		return originInvoke;
	}

	public void setOriginInvoke(String originInvoke) {
		this.originInvoke = originInvoke;
	}
	
	public void clearOriginInvoke() {
		this.originInvoke = null;
	}

	public long getOutgoingTimeout() {
		return outgoingTimeout;
	}

	public void setOutgoingTimeout( long outgoingTimeout ) {
		this.outgoingTimeout = outgoingTimeout;
	}

	/* 
	 * LIFESPAN MANAGEMENT 
	 */
	public long getLifeSpan() {
		return lifeSpan;
	}

	public void setLifeSpan(long registeredLifeSpan) {
		this.lifeSpan = registeredLifeSpan;
	}

	public void updateLifeSpan() {
		
		long now 				= System.currentTimeMillis();
		long usedTime			= now - this.getLastInstanceUpdatedTime();
		
		long remainingLifeSpan 	= TimeUnit.SECONDS.toMillis( this.getLifeSpan() ) - usedTime;
		
		remainingLifeSpan = TimeUnit.MILLISECONDS.toSeconds( remainingLifeSpan );
		remainingLifeSpan = remainingLifeSpan <= 0 ? 1 : remainingLifeSpan;
		
		this.setLifeSpan( remainingLifeSpan );
		
	}
}
