package th.co.ais.swrtc.instances;

import java.util.ArrayList;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

import org.eclipse.persistence.oxm.annotations.XmlPath;

@XmlRootElement( name = "AVP" )
@XmlAccessorType( XmlAccessType.FIELD )
public class AVP {
	
	public static String METHOD_VERSION 		= "methodVersion";
	public static String SERVER_PROVIDER_ID 	= "serverProviderId";
	public static String NETWORK_ID_DATA		= "networkIdData";
	public static String SUBSCRIBER_INFO		= "subscriberInfo";
	
	@XmlAttribute( name = "type" )
	private String type;
	
	@XmlPath( "vals/@value" )
	private ArrayList<String> value;

	public AVP() {}
	
	public AVP ( String type, String value ) {
		this.setType( type );
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public ArrayList<String> getValue() {
		return value;
	}

	public void setValue(ArrayList<String> value) {
		this.value = value;
	}
	
}
