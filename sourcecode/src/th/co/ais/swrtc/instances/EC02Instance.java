package th.co.ais.swrtc.instances;

import java.util.ArrayList;

import th.co.ais.swrtc.enums.Alarm;
import ec02.af.abstracts.AbstractAF;
import ec02.af.data.EquinoxProperties;
import ec02.af.data.EquinoxRawData;

public class EC02Instance {
	
	private EquinoxProperties properties;
	
	private AbstractAF abstractAF;
	private ArrayList<EquinoxRawData> equinoxRawDatas;
	
	private String ret;
	private String timeout;
	private String diag;
	
	private APPInstance appInstance;

	public APPInstance getAppInstance() {
		return appInstance;
	}

	public void setAppInstance(APPInstance appInstance) {
		this.appInstance = appInstance;
	}

	public ArrayList<EquinoxRawData> getEquinoxRawDatas() {
		return equinoxRawDatas;
	}

	public void setEquinoxRawDatas(ArrayList<EquinoxRawData> equinoxRawDatas) {
		this.equinoxRawDatas = equinoxRawDatas;
	}

	public EquinoxProperties getProperties() {
		return properties;
	}

	public void setProperties(EquinoxProperties properties) {
		this.properties = properties;
	}

	public AbstractAF getAbstractAF() {
		return abstractAF;
	}

	public void setAbstractAF(AbstractAF abstractAF) {
		this.abstractAF = abstractAF;
	}

	public String getRet() {
		return ret;
	}

	public void setRet(String ret) {
		this.ret = ret;
	}

	public String getTimeout() {
		return timeout;
	}

	public void setTimeout(String timeout) {
		this.timeout = timeout;
	}

	public String getDiag() {
		return diag;
	}

	public void setDiag(String diag) {
		this.diag = diag;
	}

	public void incrementsStat ( String stat ) {
		this.getAbstractAF().getUtils().incrementStats( stat );
	}
	
	public void raiseAlarm ( Alarm alarm, String[] params ) {
		this.getAbstractAF().getUtils().raiseAlarm( 
				alarm.getName(), params, 
				alarm.getSeverity(), alarm.getCategory(), alarm.getType() );
	}
	
	public void writeLog ( String name, String log  ) {
		
//		log = log.replaceAll( "_ApplicationName", ConfigureTool.getConfigure( APPConfig.APPLICATION_NAME ) );
//		
//		if ( ConfigureTool.getConfigure( AppConfig.DETAIL_LOG_NAME ).equals( name ) 
//				&& LogLevel.NO_LOG.equals( ConfigureTool.getConfigure( AppConfig.LOG_LEVEL ) ) ) {
//
//			return;
//			
//		}
		
		this.getAbstractAF().getUtils().writeLog( name, log );
		
	}
}
