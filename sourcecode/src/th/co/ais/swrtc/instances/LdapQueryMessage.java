package th.co.ais.swrtc.instances;

import java.util.ArrayList;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.google.gson.annotations.SerializedName;

@XmlRootElement( name = "LdapQueryMessage" )
@XmlAccessorType( XmlAccessType.FIELD )
public class LdapQueryMessage {
	
	@XmlElement( name="AVP" )
	@SerializedName( "AVP" )
	private ArrayList<AVP> avps;

	public ArrayList<AVP> getAvps() {
		if ( this.avps == null ) {
			this.setAvps( new ArrayList<AVP>() );
		}
		
		return avps;
	}

	public void setAvps(ArrayList<AVP> avps) {
		this.avps = avps;
	}
	
}
