package th.co.ais.swrtc.instances;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import org.eclipse.persistence.oxm.annotations.XmlPath;

@XmlRootElement( name = "MultimediaAuthen" )
@XmlAccessorType( XmlAccessType.FIELD )
public class MultimediaAuthMessage {
	
	@XmlPath( "Session-Id/@value" )
	private String sessionId;
	
	@XmlPath( "Vendor-Specific-Application-Id/@value" )
	private String vendorSpecificApplicationId;
	
	@XmlPath( "Result-Code/@value" )
	private String resultCode;
	
	@XmlPath( "Experimental-Result/@value" )
	private String experimentalResult;
	
	@XmlPath( "Auth-Session-State/@value" )
	private String authSessionState;
	
	@XmlPath( "Origin-Host/@value" )
	private String originHost;
	
	@XmlPath( "Origin-Realm/@value" )
	private String originRealm;
	
	@XmlPath( "Destination-Host/@value" )
	private String destinationHost;
	
	@XmlPath( "Destination-Realm/@value" )
	private String destinationRealm;
	
	@XmlPath( "User-Name/@value" )
	private String username;
	
	@XmlPath( "OC-Supported-Features/@value" )
	private String ocSupportedFeatures;
	
	@XmlPath( "OC-OLR/@value" )
	private String ocOLR;
	
	@XmlPath( "Supported-Features/@value" )
	private String supportedFeatures;
	
	@XmlPath( "Public-Identity/@value" )
	private String publicIdentity;

	@XmlPath( "SIP-Number-Auth-Items/@value" )
	private String SIPNumberAuthItems;

	@XmlPath( "SIP-Auth-Data-Item/@value" )
	private String SIPAuthDataItem;
	
	@XmlPath( "Server-Name/@value" )
	private String serverName;
	
	@XmlPath( "Proxy-Info/@value" )
	private String proxyInfo;
	
	@XmlPath( "Route-Record/@value" )
	private String routeRecord;
	
	public MultimediaAuthMessage () {}
	
	public MultimediaAuthMessage ( String sessionId, String vendorSpecificApplicationId, 	
			String authSessionState, String originHost, String originRealm, 
			String destinationHost, String destinationRealm, String username, 
			String ocSupportedFeatures, String supportedFeatures, String publicIdentity, 
			String SIPAuthDataItem,	String SIPNumberAuthItems, String serverName,
			String proxyInfo, String routeRecord ) {
		
		this.setSessionId( sessionId );
		this.setVendorSpecificApplicationId( vendorSpecificApplicationId );
		this.setAuthSessionState( authSessionState );
		this.setOriginHost( originHost );
		this.setOriginRealm( originRealm );
		this.setDestinationHost( destinationHost );
		this.setDestinationRealm( destinationRealm );
		this.setUsername( username );
		this.setOcSupportedFeatures( ocSupportedFeatures );
		this.setSupportedFeatures( supportedFeatures );
		this.setPublicIdentity( publicIdentity );
		this.setSIPAuthDataItem( SIPAuthDataItem );
		this.setSIPNumberAuthItems( SIPNumberAuthItems );
		this.setServerName( serverName );
		this.setProxyInfo( proxyInfo );
		this.setRouteRecord( routeRecord );
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPublicIdentity() {
		return publicIdentity;
	}

	public void setPublicIdentity(String publicIdentity) {
		this.publicIdentity = publicIdentity;
	}

	public String getResultCode() {
		return resultCode;
	}

	public void setResultCode(String resultCode) {
		this.resultCode = resultCode;
	}

	public String getVendorSpecificApplicationId() {
		return vendorSpecificApplicationId;
	}

	public void setVendorSpecificApplicationId(String vendorSpecificApplicationId) {
		this.vendorSpecificApplicationId = vendorSpecificApplicationId;
	}

	public String getExperimentalResult() {
		return experimentalResult;
	}

	public void setExperimentalResult(String experimentalResult) {
		this.experimentalResult = experimentalResult;
	}

	public String getAuthSessionState() {
		return authSessionState;
	}

	public void setAuthSessionState(String authSessionState) {
		this.authSessionState = authSessionState;
	}

	public String getOriginHost() {
		return originHost;
	}

	public void setOriginHost(String originHost) {
		this.originHost = originHost;
	}

	public String getOriginRealm() {
		return originRealm;
	}

	public String getDestinationHost() {
		return destinationHost;
	}
	
	public void setDestinationHost(String destinationHost) {
		this.destinationHost = destinationHost;
	}
	
	public String getDestinationRealm() {
		return destinationRealm;
	}
	
	public void setDestinationRealm(String destinationRealm) {
		this.destinationRealm = destinationRealm;
	}
	
	public String getServerName() {
		return serverName;
	}
	
	public void setServerName(String serverName) {
		this.serverName = serverName;
	}
	
	public void setOriginRealm(String originRealm) {
		this.originRealm = originRealm;
	}

	public String getOcSupportedFeatures() {
		return ocSupportedFeatures;
	}

	public void setOcSupportedFeatures(String ocSupportedFeatures) {
		this.ocSupportedFeatures = ocSupportedFeatures;
	}

	public String getOcOLR() {
		return ocOLR;
	}

	public void setOcOLR(String ocOLR) {
		this.ocOLR = ocOLR;
	}

	public String getSupportedFeatures() {
		return supportedFeatures;
	}

	public void setSupportedFeatures(String supportedFeatures) {
		this.supportedFeatures = supportedFeatures;
	}

	public String getSIPNumberAuthItems() {
		return SIPNumberAuthItems;
	}

	public void setSIPNumberAuthItems(String sIPNumberAuthItems) {
		SIPNumberAuthItems = sIPNumberAuthItems;
	}

	public String getSIPAuthDataItem() {
		return SIPAuthDataItem;
	}

	public void setSIPAuthDataItem(String sIPAuthDataItem) {
		SIPAuthDataItem = sIPAuthDataItem;
	}

	public String getProxyInfo() {
		return proxyInfo;
	}

	public void setProxyInfo(String proxyInfo) {
		this.proxyInfo = proxyInfo;
	}

	public String getRouteRecord() {
		return routeRecord;
	}

	public void setRouteRecord(String routeRecord) {
		this.routeRecord = routeRecord;
	}
	
	
}
