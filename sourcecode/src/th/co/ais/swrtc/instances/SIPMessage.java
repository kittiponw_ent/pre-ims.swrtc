package th.co.ais.swrtc.instances;

import java.util.ArrayList;
import java.util.HashMap;

public class SIPMessage {
	
	private static final String SIPVERSION = "SIP/2.0";
	
	private HashMap<String, ArrayList<String>> headers;
	private StringBuffer content;
	
	public SIPMessage () {
		this.setHeaders( new HashMap<String, ArrayList<String>>() );
		this.setContent( new StringBuffer() );
	}

	public StringBuffer getContent() {
		return content;
	}

	public void setContent(StringBuffer content) {
		this.content = content;
	}
	
	public void addContent( String content ) {
		this.content.append( content );
	}

	public HashMap<String, ArrayList<String>> getHeaders() {
		return headers;
	}

	public void setHeaders(HashMap<String, ArrayList<String>> _headers) {
		this.headers = _headers;
	}

	public static String getSipVersion() {
		return SIPVERSION;
	}

}