package th.co.ais.swrtc.instances;

import java.util.ArrayList;

import org.apache.commons.lang.StringEscapeUtils;

import th.co.ais.swrtc.enums.SIPRequestMethod;

public class SIPRequest extends SIPMessage {
	
	private String method;
	private String uri;
	private String version;

	public SIPRequest() {
	}

	public SIPRequest(String method, String uri, String version) {
		this.setMethod( method );
		this.setUri( uri );
		this.setVersion( version );
	}
	
	public SIPRequest ( SIPRequestMethod method, String uri ) {
		this.setMethod( method.getMethod() );
		this.setUri( uri );
		this.setVersion( SIPMessage.getSipVersion() );
	}

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	public String getUri() {
		return uri;
	}

	public void setUri(String uri) {
		this.uri = uri;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	@Override
	public String toString() {
		
		StringBuffer s = new StringBuffer();
		
		ArrayList<String> values = new ArrayList<String>();
		
		s.append( String.format( "%s %s %s\n", this.getMethod(), this.getUri(), this.getVersion() ) );
		
		for ( String key : this.getHeaders().keySet() ) {
			
			values = this.getHeaders().get( key );

//			if ( SIPHeaderFields.RECORD_ROUTE.equals( key ) || SIPHeaderFields.ROUTE.equals( key ) ) {
//				s.append( key + ": " +  StringUtils.join( values, "," ) + " " + "\n" );
//				
//			}
//			else {
				for ( String value : values ) {
					s.append( key + ": " + value + "\r\n");
					
				}
//			}
		}
		
		s.append( "\r\n" );
		s.append( this.getContent() );
			
		return StringEscapeUtils.escapeHtml( s.toString().trim() );
	}
	
}
