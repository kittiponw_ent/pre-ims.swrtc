package th.co.ais.swrtc.instances;

import java.util.ArrayList;

import org.apache.commons.lang.StringEscapeUtils;

import th.co.ais.swrtc.enums.SIPResponseCode;


public class SIPResponse extends SIPMessage {
	
	private String version;
	private String code;
	private String reason;

	public SIPResponse() {
	}

	public SIPResponse(String version, String code, String reason) {
		this.setVersion( version );
		this.setCode( code );
		this.setReason( reason );
	}
	
	public SIPResponse( SIPResponseCode responseCode ) {
		this.setVersion( SIPMessage.getSipVersion() );
		this.setCode( responseCode.getCode() );
		this.setReason( responseCode.getReason() );
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	@Override
	public String toString() {
		
 		StringBuffer s = new StringBuffer();
		
		ArrayList<String> values = new ArrayList<String>();
		
		s.append( String.format( "%s %s %s\n", this.getVersion(), this.getCode(), this.getReason() ) );
		
		for ( String key : this.getHeaders().keySet() ) {
			
			values = this.getHeaders().get( key );

//			if ( SIPHeaderFields.RECORD_ROUTE.equals( key ) || SIPHeaderFields.ROUTE.equals( key ) ) {
//				s.append( key + ": " +  StringUtils.join( values, "," ) + " " + "\n" );
//				
//			}
//			else {
				for ( String value : values ) {
					s.append( key + ": " + value + "\r\n");
					
				}
//			}
		}
		
		s.append( "\r\n" );
		s.append( this.getContent() );
			
		return StringEscapeUtils.escapeHtml( s.toString().trim() );
	}

}
