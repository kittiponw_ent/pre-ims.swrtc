package th.co.ais.swrtc.instances;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import org.eclipse.persistence.oxm.annotations.XmlPath;

@XmlRootElement( name = "ServerAssignmentMessage" )
@XmlAccessorType( XmlAccessType.FIELD )
public class ServerAssignmentMessage {

	@XmlPath( "Session-Id/@value" )
	private String sessionId;
	
	@XmlPath( "Vendor-Specific-Application-Id/@value" )
	private String vendorSpecificApplicationId;
	
	@XmlPath( "Auth-Session-State/@value" )
	private String authSessionState;
	
	@XmlPath( "Result-Code/@value" )
	private String resultCode;
	
	@XmlPath( "Experimental-Result/@value" )
	private String experimentalResult;
	
	@XmlPath( "Origin-Host/@value" )
	private String originHost;
	
	@XmlPath( "Origin-Realm/@value" )
	private String originRealm;
	
	@XmlPath( "Destination-Host/@value" )
	private String destinationHost;
	
	@XmlPath( "Destination-Realm/@value" )
	private String destinationRealm;
	
	@XmlPath( "User-Name/@value" )
	private String username;
	
	@XmlPath( "OC-Supported-Features/@value" )
	private String ocSupportedFeatures;
	
	@XmlPath( "OC-OLR/@value" )
	private String ocOLR;
	
	@XmlPath( "Supported-Features/@value" )
	private String supportedFeatures;
	
	@XmlPath( "User-Data/@value" )
	private String userData;
	
	@XmlPath( "Charging-Information/@value" )
	private String chargingInformation;
	
	@XmlPath( "Associated-Identities/@value" )
	private String associatedIdentities;
	
	@XmlPath( "Loose-Route-Indication/@value" )
	private String looseRouteIndication;
	
	@XmlPath( "Associated-Registered-Identities/@value" )
	private String associatedRegisteredIdentities;
	
	@XmlPath( "Public-Identity/@value" )
	private String publicIdentity;
	
	@XmlPath( "Wildcarded-Public-Identity/@value" )
	private String wildcardedPublicIdentity;
	
	@XmlPath( "Server-Name/@value" )
	private String servername;
	
	@XmlPath( "Server-Assignment-Type/@value" )
	private String serverAssignmentType;
	
	@XmlPath( "User-Data-Already-Available/@value" )
	private String userDataAlreadyAvailable;
	
	@XmlPath( "SCSCF-Restoration-Info/@value" )
	private String sCSCFRestorationInfo;
	
	@XmlPath( "Multiple-Registration-Indication/@value" )
	private String multipleRegistrationIndication;
	
	@XmlPath( "Session-Priority/@value" )
	private String sessionPriority;
	
	@XmlPath( "Priviledged-Sender-Indication/@value" )
	private String priviledgedSenderIndication;
	
	@XmlPath( "Proxy-Info/@value" )
	private String proxyInfo;
	
	@XmlPath( "Route-Record/@value" )
	private String routeRecord;
	
	public ServerAssignmentMessage () {}
	
	public ServerAssignmentMessage ( String sessionId, String vendorSpecificApplicationId, 	
			String authSessionState, String originHost, String originRealm, 
			String destinationHost, String destinationRealm, String username, 
			String ocSupportedFeatures, String supportedFeatures, String publicIdentity, 
			String wildcardedPublicIdentity, String serverName, String serverAssignmentType,
			String userDataAlreadyAvailable, String sCSCFRestorationInfo, 
			String multipleRegistrationIndication, String sessionPriority,
			String proxyInfo, String routeRecord ) {
		this.setSessionId( sessionId );
		this.setVendorSpecificApplicationId( vendorSpecificApplicationId );
		this.setAuthSessionState( authSessionState );
		this.setOriginHost( originHost );
		this.setOriginRealm( originRealm );
		this.setDestinationHost( destinationHost );
		this.setDestinationRealm( destinationRealm );
		this.setUsername( username );
		this.setOcSupportedFeatures( ocSupportedFeatures );
		this.setSupportedFeatures( supportedFeatures );
		this.setPublicIdentity( publicIdentity );
		this.setWildcardedPublicIdentity( wildcardedPublicIdentity );
		this.setServername( serverName );
		this.setServerAssignmentType( serverAssignmentType );
		this.setUserDataAlreadyAvailable( userDataAlreadyAvailable );
		this.setsCSCFRestorationInfo( sCSCFRestorationInfo );
		this.setMultipleRegistrationIndication( multipleRegistrationIndication );
		this.setSessionPriority( sessionPriority );
		this.setProxyInfo( proxyInfo );
		this.setRouteRecord( routeRecord );
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getServername() {
		return servername;
	}

	public void setServername(String servername) {
		this.servername = servername;
	}

	public String getPublicIdentity() {
		return publicIdentity;
	}

	public void setPublicIdentity(String publicIdentity) {
		this.publicIdentity = publicIdentity;
	}

	public String getResultCode() {
		return resultCode;
	}

	public void setResultCode(String resultCode) {
		this.resultCode = resultCode;
	}

	public String getServerAssignmentType() {
		return serverAssignmentType;
	}

	public void setServerAssignmentType(String serverAssignmentType) {
		this.serverAssignmentType = serverAssignmentType;
	}

	public String getVendorSpecificApplicationId() {
		return vendorSpecificApplicationId;
	}

	public void setVendorSpecificApplicationId(String vendorSpecificApplicationId) {
		this.vendorSpecificApplicationId = vendorSpecificApplicationId;
	}

	public String getAuthSessionState() {
		return authSessionState;
	}

	public void setAuthSessionState(String authSessionState) {
		this.authSessionState = authSessionState;
	}

	public String getExperimentalResult() {
		return experimentalResult;
	}

	public void setExperimentalResult(String experimentalResult) {
		this.experimentalResult = experimentalResult;
	}

	public String getOriginHost() {
		return originHost;
	}

	public void setOriginHost(String originHost) {
		this.originHost = originHost;
	}

	public String getOriginRealm() {
		return originRealm;
	}

	public void setOriginRealm(String originRealm) {
		this.originRealm = originRealm;
	}

	public String getDestinationHost() {
		return destinationHost;
	}

	public void setDestinationHost(String destinationHost) {
		this.destinationHost = destinationHost;
	}

	public String getDestinationRealm() {
		return destinationRealm;
	}

	public void setDestinationRealm(String destinationRealm) {
		this.destinationRealm = destinationRealm;
	}

	public String getOcSupportedFeatures() {
		return ocSupportedFeatures;
	}

	public void setOcSupportedFeatures(String ocSupportedFeatures) {
		this.ocSupportedFeatures = ocSupportedFeatures;
	}

	public String getOcOLR() {
		return ocOLR;
	}

	public void setOcOLR(String ocOLR) {
		this.ocOLR = ocOLR;
	}

	public String getSupportedFeatures() {
		return supportedFeatures;
	}

	public void setSupportedFeatures(String supportedFeatures) {
		this.supportedFeatures = supportedFeatures;
	}

	public String getUserData() {
		return userData;
	}

	public void setUserData(String userData) {
		this.userData = userData;
	}

	public String getChargingInformation() {
		return chargingInformation;
	}

	public void setChargingInformation(String chargingInformation) {
		this.chargingInformation = chargingInformation;
	}

	public String getAssociatedIdentities() {
		return associatedIdentities;
	}

	public void setAssociatedIdentities(String associatedIdentities) {
		this.associatedIdentities = associatedIdentities;
	}

	public String getLooseRouteIndication() {
		return looseRouteIndication;
	}

	public void setLooseRouteIndication(String looseRouteIndication) {
		this.looseRouteIndication = looseRouteIndication;
	}

	public String getAssociatedRegisteredIdentities() {
		return associatedRegisteredIdentities;
	}

	public void setAssociatedRegisteredIdentities(
			String associatedRegisteredIdentities) {
		this.associatedRegisteredIdentities = associatedRegisteredIdentities;
	}

	public String getWildcardedPublicIdentity() {
		return wildcardedPublicIdentity;
	}

	public void setWildcardedPublicIdentity(String wildcardedPublicIdentity) {
		this.wildcardedPublicIdentity = wildcardedPublicIdentity;
	}

	public String getUserDataAlreadyAvailable() {
		return userDataAlreadyAvailable;
	}

	public void setUserDataAlreadyAvailable(String userDataAlreadyAvailable) {
		this.userDataAlreadyAvailable = userDataAlreadyAvailable;
	}

	public String getsCSCFRestorationInfo() {
		return sCSCFRestorationInfo;
	}

	public void setsCSCFRestorationInfo(String sCSCFRestorationInfo) {
		this.sCSCFRestorationInfo = sCSCFRestorationInfo;
	}

	public String getMultipleRegistrationIndication() {
		return multipleRegistrationIndication;
	}

	public void setMultipleRegistrationIndication(
			String multipleRegistrationIndication) {
		this.multipleRegistrationIndication = multipleRegistrationIndication;
	}

	public String getSessionPriority() {
		return sessionPriority;
	}

	public void setSessionPriority(String sessionPriority) {
		this.sessionPriority = sessionPriority;
	}

	public String getPriviledgedSenderIndication() {
		return priviledgedSenderIndication;
	}

	public void setPriviledgedSenderIndication(String priviledgedSenderIndication) {
		this.priviledgedSenderIndication = priviledgedSenderIndication;
	}

	public String getProxyInfo() {
		return proxyInfo;
	}

	public void setProxyInfo(String proxyInfo) {
		this.proxyInfo = proxyInfo;
	}

	public String getRouteRecord() {
		return routeRecord;
	}

	public void setRouteRecord(String routeRecord) {
		this.routeRecord = routeRecord;
	}
	
	
	
	
}
