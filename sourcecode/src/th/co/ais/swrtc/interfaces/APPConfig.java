package th.co.ais.swrtc.interfaces;

public interface APPConfig {
	
	String APPLICATION_NAME					= "Application-Name";

	String RESPONSE_TIME_LIMIT				= "Response-Time-Limit";
	String MAXIMUM_WAIT_TIME_FOR_2ND_REGIS	= "Maximum-Wait-Time-For-2nd-Register";
	
	String MAAA_INTERFACE					= "MAAA-Interface";
	String MNPDB_INTERFACE					= "MNPDB-Interface";
	String IWRTC_INTERFACE					= "IWRTC-Interface";
	String PWRTC_INTERFACE					= "PWRTC-Interface";
	String TERM_SWRTC_INTERFACE				= "TERM-SWRTC-Interface";
	String AS_INTERFACE 					= "AS-Interface";
	
}
