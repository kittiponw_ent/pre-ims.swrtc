package th.co.ais.swrtc.interfaces;

public interface ActionType {
	
	String CALLER 		= "CALLER";
	String CALLEE 		= "CALLEE";
	
}
