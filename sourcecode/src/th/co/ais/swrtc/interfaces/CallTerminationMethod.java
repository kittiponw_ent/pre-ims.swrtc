package th.co.ais.swrtc.interfaces;

public interface CallTerminationMethod {
	
	String BYE_FROM_AS				= "WAIT_INVITE_FROM_AS";
	String BYE_FROM_HERE			= "BYE_FROM_HERE";
	String BYE_FROM_OPPOSITE		= "BYE_FROM_OPPOSITE";
	
}
