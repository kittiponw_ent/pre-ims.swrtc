package th.co.ais.swrtc.interfaces;

public interface Event {
	
	String WAIT_INVITE_FROM_AS							= "WAIT_INVITE_FROM_AS";
	String WAIT_100_FROM_I								= "WAIT_100_FROM_I";
	String WAIT_183										= "WAIT_183_FROM_I";
	
	String WAIT_200_FROM_AS								= "WAIT_200_FROM_AS";
	String WAIT_ACK_FROM_P								= "WAIT_ACK_FROM_P";		
	String WAIT_ACK_FROM_AS								= "WAIT_ACK_FROM_AS";
	String WAIT_ACK_FROM_I								= "WAIT_ACK_FROM_I";

	// USE IN INITIATE_TERMINATION STATE
	String WAIT_BYE_FROM_AS								= "WAIT_BYE_FROM_AS";
	String WAIT_200_FROM_BOTH_SIDE						= "WAIT_200_FROM_BOTH_SIDE";
	String WAIT_200_FROM_OPPOSITE_S						= "WAIT_200_FROM_OPPOSITE_S";
	String WAIT_200_AFTER_BYE							= "WAIT_200_AFTER_BYE";
	
	String WAIT_200_FROM_P								= "WAIT_200_FROM_P";
	
	String WAIT_200_FROM_P_AND_200_FROM_OPPOSITE_S		= "WAIT_200_FROM_P_AND_200_FROM_OPPOSITE_S";
	
}
