package th.co.ais.swrtc.interfaces;

public interface MessageType {
	
	public String REQUEST 		= "request";
	public String RESPONSE 		= "response";
	
}
