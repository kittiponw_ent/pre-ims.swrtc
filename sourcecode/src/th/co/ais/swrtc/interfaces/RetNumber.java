package th.co.ais.swrtc.interfaces;

public interface RetNumber {

	String NORMAL 		= "0";
	String ERROR 		= "1";
	String REJECT 		= "2";
	String ABORT 		= "3";
	String TIMEOUT 		= "4";
	String FAIL 		= "5";
	String END 			= "10";
}
