package th.co.ais.swrtc.interfaces;

public interface SubStates {

	String WAIT_PRACK_200				= "W_PRACK_200";
	
	String WAIT_UPDATE					= "W_UPDATE";
	String WAIT_UPDATE_200				= "W_UPDATE_200";
	
}