package th.co.ais.swrtc.interfaces;

public interface TerminationCode {

	// SUCCESS
	String SUCCESS		= "S0";
	
	// NO-FLOW
	String TIMEOUT 		= "T0";
	String REJECT 		= "R0";
	String ERROR 		= "E0";
	String ABORT 		= "A0";
	
}
