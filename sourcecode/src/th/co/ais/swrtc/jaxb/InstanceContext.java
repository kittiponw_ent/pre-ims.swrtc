package th.co.ais.swrtc.jaxb;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;

import th.co.ais.swrtc.instances.LdapQueryMessage;
import th.co.ais.swrtc.instances.MultimediaAuthMessage;
import th.co.ais.swrtc.instances.ServerAssignmentMessage;

public class InstanceContext {
	
	private static JAXBContext multimediaAuthContext;
	private static JAXBContext serverAssignmentContext;
	private static JAXBContext ldataQueryMessageContext;
	
	//
	public static synchronized void initMultimediaAuthContext() throws JAXBException {
		if ( multimediaAuthContext == null )
			multimediaAuthContext = JAXBContext.newInstance( MultimediaAuthMessage.class );
	}
	
	public static JAXBContext getMultimediaAuthContext() {
		return multimediaAuthContext;
	}
	
	// 
	public static synchronized void initServerAssignmentContext() throws JAXBException {
		if ( serverAssignmentContext == null )
			serverAssignmentContext = JAXBContext.newInstance( ServerAssignmentMessage.class );
	}
	
	public static JAXBContext getServerAssignmentContext() {
		return serverAssignmentContext;
	}
	
	//
	public static synchronized void initLdapQueryMessageContext() throws JAXBException {
		if ( ldataQueryMessageContext == null )
			ldataQueryMessageContext = JAXBContext.newInstance( LdapQueryMessage.class );
	}
	
	public static JAXBContext getLdapQueryMessageContext() {
		return ldataQueryMessageContext;
	}
	
}
