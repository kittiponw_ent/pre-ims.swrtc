package th.co.ais.swrtc.jaxb;

import java.io.StringReader;
import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

public class JAXBHandler {
	
	// IMPORT MESSAGE TO INSTANCE [ UNMARSHALLING ]
	public static Object createInstance ( JAXBContext context, String message, Class<?> instanceClass ) {

		Object instance = null;
		
		Unmarshaller unmarshaller;
		try {
			unmarshaller = context.createUnmarshaller();
			
			instance = Class.forName( instanceClass.getCanonicalName() ).newInstance();
			instance = unmarshaller.unmarshal( new StringReader(message) );
			
		} catch( Exception e ) {
			e.printStackTrace();
			return null;
		}
		
		return instance;
	}
	
	// EXPORT INSTANCE TO MESSAGE [ MARSHALLING ]
	public static String composeMessage ( JAXBContext context, Object instance ) {
		
		StringWriter writer = new StringWriter();
		
		Marshaller marshaller;
		try {
			marshaller = context.createMarshaller();
			marshaller.setProperty( Marshaller.JAXB_FORMATTED_OUTPUT, true );
			marshaller.setProperty( Marshaller.JAXB_FRAGMENT, true );
			
			marshaller.marshal( instance, writer );
			
		} catch( JAXBException e ) { 
			e.printStackTrace();
		}
		
		return writer.toString();
	}
}
