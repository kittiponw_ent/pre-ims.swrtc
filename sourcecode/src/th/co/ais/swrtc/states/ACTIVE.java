package th.co.ais.swrtc.states;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;

import th.co.ais.swrtc.enums.Alarm;
import th.co.ais.swrtc.enums.EquinoxEvent;
import th.co.ais.swrtc.enums.SIPRequestMethod;
import th.co.ais.swrtc.enums.SIPResponseCode;
import th.co.ais.swrtc.exception.SIPParseException;
import th.co.ais.swrtc.exception.VerificationFailedException;
import th.co.ais.swrtc.instances.APPInstance;
import th.co.ais.swrtc.instances.AVP;
import th.co.ais.swrtc.instances.EC02Instance;
import th.co.ais.swrtc.instances.LdapQueryMessage;
import th.co.ais.swrtc.instances.SIPMessage;
import th.co.ais.swrtc.instances.SIPRequest;
import th.co.ais.swrtc.interfaces.APPConfig;
import th.co.ais.swrtc.interfaces.ActionType;
import th.co.ais.swrtc.interfaces.Event;
import th.co.ais.swrtc.interfaces.MessageType;
import th.co.ais.swrtc.interfaces.RetNumber;
import th.co.ais.swrtc.interfaces.States;
import th.co.ais.swrtc.jaxb.InstanceContext;
import th.co.ais.swrtc.jaxb.JAXBHandler;
import th.co.ais.swrtc.utils.AppDetail;
import th.co.ais.swrtc.utils.AppDetail.InputDetail;
import th.co.ais.swrtc.utils.AppDetail.OutputDetail;
import th.co.ais.swrtc.utils.AppDetailEvent;
import th.co.ais.swrtc.utils.ConfigureTool;
import th.co.ais.swrtc.utils.IDGenerator;
import th.co.ais.swrtc.utils.SIPHeaderFields;
import th.co.ais.swrtc.utils.SIPParser;
import th.co.ais.swrtc.verificator.SIPVerificator;

import com.google.gson.GsonBuilder;

import ec02.af.abstracts.AbstractAF;
import ec02.af.data.EquinoxRawData;
import ec02.af.interfaces.IAFState;

public class ACTIVE implements IAFState {
	
	private ArrayList<EquinoxRawData> outputs;
	
	private EC02Instance ec02Instance;
	
	private APPInstance appInstance;
	private AppDetail detail;
	
	private String currentState;
	private String nextState;

	@Override
	public String doAction( AbstractAF af, Object instance, ArrayList<EquinoxRawData> rawDatas ) {
		
		long transactionStartTime = System.currentTimeMillis();
		
		this.ec02Instance = ( EC02Instance ) instance;
		this.appInstance = this.ec02Instance.getAppInstance();
		
		this.currentState = this.ec02Instance.getProperties().getState();
		
		this.outputs = new ArrayList<EquinoxRawData>();
		
		this.detail = new AppDetail();
		this.detail.setCurrentState( this.ec02Instance.getProperties().getState() );
		this.detail.getTransactionDetail().setSession( "equinox-session" );
		this.detail.getTransactionDetail().setInitialInvoke( "initial-invoke" );
		
		EquinoxRawData r = rawDatas.get( 0 );
		
		/* NORMAL FLOW */
		if ( RetNumber.NORMAL.equals( r.getRet() ) ) {
			
			String normalRawDataMessage = r.getRawDataCDATAAttributes( "val" );
		
			try {
				
				this.appInstance.setOrigin( r.getOrig() );
				this.appInstance.setOriginInvoke( r.getInvoke() );
				
				// IODETAIL - INCOMING INVITE MESSAGE
				InputDetail normalInputDetail = this.detail.new InputDetail(
						r.getInvoke(),
						new AppDetailEvent( r.getOrig(), "Command.." ),
						r.getType(),
						r.getRawDataAttributes(),
						normalRawDataMessage, null
						);
				
				SIPMessage sip = SIPParser.getMessage( normalRawDataMessage );
				
				normalInputDetail.setData( sip );
				
				this.detail.getTransactionDetail().addInput( normalInputDetail );
				this.detail.getTransactionDetail().setInputTimeStamp( transactionStartTime );
				
				// VERIRY 'INVITE' MESSAGE
				SIPVerificator.verify( SIPRequestMethod.INVITE, sip );
				
				String recordRoute = StringEscapeUtils.unescapeHtml( 
						sip.getHeaders().get( SIPHeaderFields.RECORD_ROUTE ).get( 0 )
						);
				
				HashMap<String, String> attrs = new HashMap<String, String>();
				
				EquinoxRawData output = new EquinoxRawData();
				
				// INVITATION FROM PWRTC ( ACT AS 'CALLER' )
				if ( StringUtils.containsIgnoreCase( recordRoute, "pwrtc" ) ) {
					
					// GENERATE LDAP QUERY TO MNPDB
					attrs.put( "name", "LDAP" );
					attrs.put( "ctype", "extended" );
					attrs.put( "type", MessageType.REQUEST );
					attrs.put( "to", ConfigureTool.getConfigure( APPConfig.MNPDB_INTERFACE ) );
					attrs.put( "oid", "0.0.17.1218.8.7.0" );
					
					ArrayList<AVP> avpList = new ArrayList<AVP>();
					avpList.add( new AVP( AVP.METHOD_VERSION, "7" ) );
					avpList.add( new AVP( AVP.SERVER_PROVIDER_ID, "66923029233" ) );
					avpList.add( new AVP( AVP.NETWORK_ID_DATA, "66923029233" ) );
					
					LdapQueryMessage ldapQuery = new LdapQueryMessage();
					ldapQuery.setAvps( avpList );
					
					output.setInvoke( IDGenerator.generateUniqueId() );
					output.setRawMessage( 
							JAXBHandler.composeMessage( 
									InstanceContext.getLdapQueryMessageContext(), 
									ldapQuery ).replaceAll( "</*LdapQueryMessage>", "" ).trim()
							);
					output.setRawDataAttributes( attrs );
					
					this.appInstance.setActionType( ActionType.CALLER );
					this.appInstance.setSIPMessage( sip );
					this.appInstance.addPendingRequest( output.getInvoke() );
					
					/*
					 * KEEP UPDATED LIFESPAN INTO APP-INSTANCE
					 * 
					 */
					this.appInstance.updateLifeSpan();
					
					this.ec02Instance.setTimeout( ConfigureTool.getConfigure( APPConfig.RESPONSE_TIME_LIMIT ) );
					
					this.nextState = States.WAIT_NETWORK_OPERATOR;
					
					// IO DETAIL - OUTGOING LDAP QUERY
					OutputDetail OutputDetail = this.detail.new OutputDetail(
							output.getInvoke(),
							new AppDetailEvent( "Application", "Network Operator" ),
							output.getType(),
							output.getRawDataAttributes(),
							output.getRawDataMessage()
							);
					
					OutputDetail.setData( ldapQuery );
					
					this.detail.getTransactionDetail().addOutput( OutputDetail );
					
				}
				// INVITATION FROM IWRTC ( ACT AS 'CALLEE' )
				else if ( StringUtils.containsIgnoreCase( recordRoute, "iWRTC" ) ) {
					
					String outputInvitation = ( ( SIPRequest ) sip ).toString();
					
					// FORWARD SIP MESSAGE TO AS
					attrs.put( "name", "SOCKET" );
					attrs.put( "ctype", "udp" );
					attrs.put( "type", MessageType.REQUEST );
					attrs.put( "to", ConfigureTool.getConfigure( APPConfig.AS_INTERFACE ) );
					attrs.put( "val", outputInvitation.toString() );
					
					output.setInvoke( IDGenerator.generateUniqueId() );
					output.setRawDataAttributes( attrs );
					
					this.appInstance.setActionType( ActionType.CALLEE );
					this.appInstance.addPendingRequest( output.getInvoke() );
					this.appInstance.setEvent( Event.WAIT_INVITE_FROM_AS );
					
					this.nextState = States.INITIATE_INVITE;
					
					// IO DETAIL - OUTGOING SIP INVITE
					OutputDetail OutputDetail = this.detail.new OutputDetail(
							output.getInvoke(),
							new AppDetailEvent( "Application", "" ),
							output.getType(),
							output.getRawDataAttributes(),
							outputInvitation
							);
					
					OutputDetail.setData( null );
					
					this.detail.getTransactionDetail().addOutput( OutputDetail );
					
				}
				// INVITATION ERROR
				else {
					
				}
				
				this.outputs.add( output );
				
			}
			catch ( SIPParseException e ) {
				
				HashMap<String, String> attrs = new HashMap<String, String>();
				attrs.put( "name", "SOCKET" );
				attrs.put( "ctype", "udp" );
				attrs.put( "type", MessageType.RESPONSE );
				attrs.put( "to", this.appInstance.getOrigin() );
				attrs.put( "val", " SIP PARSE EXCEPTION " );
				
				EquinoxRawData output = new EquinoxRawData();
				output.setInvoke( this.appInstance.getOriginInvoke() );
				output.setRawDataAttributes( attrs );
				
				this.appInstance.clearOrigin();
				this.appInstance.clearOriginInvoke();
				
				this.appInstance.updateLifeSpan();
				
				this.ec02Instance.setTimeout( 
						Long.toString( this.appInstance.getLifeSpan() )
						);
				
				this.nextState = States.ACTIVE;
				
			}
			catch ( VerificationFailedException e ) {
				
				HashMap<String, String> attrs = new HashMap<String, String>();
				attrs.put( "name", "SOCKET" );
				attrs.put( "ctype", "udp" );
				attrs.put( "type", MessageType.RESPONSE );
				attrs.put( "to", this.appInstance.getOrigin() );
				attrs.put( "val", " INVITE VERIFICATION FAILED EXCEPTION " );
				
				EquinoxRawData output = new EquinoxRawData();
				output.setInvoke( this.appInstance.getOriginInvoke() );
				output.setRawDataAttributes( attrs );
				
				this.appInstance.clearOrigin();
				this.appInstance.clearOriginInvoke();
				
				this.appInstance.updateLifeSpan();
				
				this.ec02Instance.setTimeout( 
						Long.toString( this.appInstance.getLifeSpan() )
						);
				
				this.nextState = States.ACTIVE;
				
			}
			
		} 
		/* NO FLOW : TIMEOUT ONLY */
		else {
			
			String noFlowStatistic = null;
			Alarm noFlowAlarm = null;
			
			EquinoxEvent e = EquinoxEvent.getEquinoxEventFrom( r.getRet() );
			
			SIPResponseCode noFlowResponseCode = null;
			
			switch ( e ) {
			case ERROR:
				noFlowStatistic				= null;
				noFlowAlarm					= null;
				noFlowResponseCode			= SIPResponseCode.SERVER_INTERNAL_ERROR;
				break;
				
			case REJECT:
				noFlowStatistic				= null;
				noFlowAlarm					= null;
				noFlowResponseCode			= SIPResponseCode.SERVER_INTERNAL_ERROR;
				break;
				
			case ABORT:
				noFlowStatistic				= null;
				noFlowAlarm					= null;
				noFlowResponseCode			= SIPResponseCode.SERVER_INTERNAL_ERROR;
				break;
				
			case TIMEOUT:
				noFlowStatistic				= null;
				noFlowAlarm					= null;
				noFlowResponseCode			= SIPResponseCode.SERVER_INTERNAL_ERROR;
				break;
				
			default:
				break;
			}
			
			// IO DETAIL - NO FLOW INPUT DETAIL
			InputDetail noFlowInputDetail = this.detail.new InputDetail(
					r.getInvoke(),
					new AppDetailEvent( "App", "Event" ),
					r.getType(),
					r.getRawDataAttributes(),
					r.getRawDataAttribute( "val" ),
					transactionStartTime - this.appInstance.getLastInstanceUpdatedTime()
					);
			
			noFlowInputDetail.setData( null );
			
			this.detail.getTransactionDetail().addInput( noFlowInputDetail );
			this.detail.getTransactionDetail().setInputTimeStamp( transactionStartTime );
			
			this.ec02Instance.incrementsStat( noFlowStatistic );
			this.ec02Instance.raiseAlarm( noFlowAlarm, new String[]{} );
			
			this.nextState = States.IDLE;
			
		}
		
		/* */
		long transactionEndTime = System.currentTimeMillis();
		
		this.appInstance.setLastInstanceUpdatedTime( transactionEndTime );
		
		this.ec02Instance.setEquinoxRawDatas( this.outputs );
		this.ec02Instance.setRet( RetNumber.NORMAL );
		
		this.detail.getTransactionDetail().setOutputTimeStamp( transactionEndTime );
		
		this.detail.setNextState( this.nextState );
		this.detail.setProcessingTime( transactionEndTime - transactionStartTime );
		
		System.out.println( "---" );
		System.out.println(
				new GsonBuilder().setPrettyPrinting().disableHtmlEscaping().create().toJson( this.detail )
				);
		
		return this.nextState;
	}

}
