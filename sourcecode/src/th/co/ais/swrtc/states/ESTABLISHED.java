package th.co.ais.swrtc.states;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.commons.lang.StringUtils;

import th.co.ais.swrtc.enums.Alarm;
import th.co.ais.swrtc.enums.EquinoxEvent;
import th.co.ais.swrtc.enums.SIPRequestMethod;
import th.co.ais.swrtc.enums.SIPResponseCode;
import th.co.ais.swrtc.exception.SIPParseException;
import th.co.ais.swrtc.instances.APPInstance;
import th.co.ais.swrtc.instances.EC02Instance;
import th.co.ais.swrtc.instances.SIPMessage;
import th.co.ais.swrtc.instances.SIPRequest;
import th.co.ais.swrtc.instances.SIPResponse;
import th.co.ais.swrtc.instances.ServerAssignmentMessage;
import th.co.ais.swrtc.interfaces.APPConfig;
import th.co.ais.swrtc.interfaces.CallTerminationMethod;
import th.co.ais.swrtc.interfaces.Event;
import th.co.ais.swrtc.interfaces.MessageType;
import th.co.ais.swrtc.interfaces.RetNumber;
import th.co.ais.swrtc.interfaces.States;
import th.co.ais.swrtc.jaxb.InstanceContext;
import th.co.ais.swrtc.jaxb.JAXBHandler;
import th.co.ais.swrtc.utils.AppDetail;
import th.co.ais.swrtc.utils.AppDetailEvent;
import th.co.ais.swrtc.utils.ConfigureTool;
import th.co.ais.swrtc.utils.IDGenerator;
import th.co.ais.swrtc.utils.SIPHeaderFields;
import th.co.ais.swrtc.utils.SIPParser;
import th.co.ais.swrtc.utils.AppDetail.InputDetail;
import th.co.ais.swrtc.utils.AppDetail.OutputDetail;

import com.google.gson.GsonBuilder;

import ec02.af.abstracts.AbstractAF;
import ec02.af.data.EquinoxRawData;
import ec02.af.interfaces.IAFState;

public class ESTABLISHED implements IAFState {
	
	private ArrayList<EquinoxRawData> outputs;
	
	private EC02Instance ec02Instance;
	
	private APPInstance appInstance;
	private AppDetail detail;
	
	private String currentState;
	private String nextState;

	@Override
	public String doAction( AbstractAF af, Object instance, ArrayList<EquinoxRawData> rawDatas ) {
		
		long transactionStartTime = System.currentTimeMillis();
		
		this.ec02Instance = ( EC02Instance ) instance;
		this.appInstance = this.ec02Instance.getAppInstance();
		
		this.currentState = this.ec02Instance.getProperties().getState();
		
		this.outputs = new ArrayList<EquinoxRawData>();
		
		this.detail = new AppDetail();
		this.detail.setCurrentState( this.ec02Instance.getProperties().getState() );
		this.detail.getTransactionDetail().setSession( "equinox-session" );
		this.detail.getTransactionDetail().setInitialInvoke( "initial-invoke" );
		
		EquinoxRawData r = rawDatas.get( 0 );
		
		/* NORMAL FLOW */
		if ( RetNumber.NORMAL.equals( r.getRet() ) ) {
			
			String normalRawDataMessage = r.getRawDataCDATAAttributes( "val" );
			
			try {
				
				SIPMessage sip = SIPParser.getMessage( normalRawDataMessage );
				
				if ( sip instanceof SIPRequest ) {
					
					// VERIFY SIP
					
					SIPRequestMethod requestMethod = SIPRequestMethod.getSipRequestMethod( ( ( SIPRequest ) sip ).getMethod() );
					
					String recordRoute = sip.getHeaders().get( SIPHeaderFields.RECORD_ROUTE ).get( 0 );
					
					switch ( requestMethod ) {
					case BYE:
						
						// TERMINATION FROM P OR S
						if ( StringUtils.containsIgnoreCase( recordRoute, "pwrtc" )
								|| StringUtils.containsIgnoreCase( recordRoute, "swrtc" ) ) {
							
							HashMap<String, String> attrs = new HashMap<String, String>();
							attrs.put( "name", "SOCKET" );
							attrs.put( "ctype", "udp" );
							attrs.put( "type", MessageType.REQUEST );
							attrs.put( "to", ConfigureTool.getConfigure( APPConfig.AS_INTERFACE ) );
							attrs.put( "val", sip.toString() );
							
							EquinoxRawData output = new EquinoxRawData();
							output.setInvoke( IDGenerator.generateUniqueId() );
							output.setRawDataAttributes( attrs );
							
							this.outputs.add( output );
							
							this.appInstance.addPendingRequest( output.getInvoke() );
							this.appInstance.setEvent( Event.WAIT_BYE_FROM_AS );
							this.appInstance.setCallTerminationMethod(
									StringUtils.containsIgnoreCase( recordRoute, "pwrtc" ) 
									? CallTerminationMethod.BYE_FROM_HERE 
									: CallTerminationMethod.BYE_FROM_OPPOSITE
									);
							
							this.ec02Instance.setTimeout( "420" );
							
							this.nextState = States.INITIATE_TERMINATION;
							
						}
						// TERMINATION FROM AS
						else {
							
							HashMap<String, String> attrs = new HashMap<String, String>();
							attrs.put( "name", "SOCKET" );
							attrs.put( "ctype", "udp" );
							attrs.put( "type", MessageType.REQUEST );
							attrs.put( "val", sip.toString() );
							
							EquinoxRawData output;
							
							String[] dest = { 
									ConfigureTool.getConfigure( APPConfig.PWRTC_INTERFACE ), 
									ConfigureTool.getConfigure( APPConfig.TERM_SWRTC_INTERFACE ) 
									};

							// FORWARD TERMINATION TO P & S
							for (String d : dest) {
								
								attrs.put( "to", d );
								
								output = new EquinoxRawData();
								output.setInvoke( IDGenerator.generateUniqueId() );
								output.setRawDataAttributes( attrs );
								
								this.outputs.add( output );
								
								this.appInstance.addPendingRequest( output.getInvoke() );
								
							}
							
							this.appInstance.setEvent( Event.WAIT_200_FROM_BOTH_SIDE );
							this.appInstance.setCallTerminationMethod( CallTerminationMethod.BYE_FROM_AS );
							
							this.ec02Instance.setTimeout( ConfigureTool.getConfigure( APPConfig.RESPONSE_TIME_LIMIT ) );
							
							this.nextState = States.INITIATE_TERMINATION;
							
						}
						
						break;

					default:
						break;
					}
					
				}
			} catch (SIPParseException e) {
				e.printStackTrace();
			}
			
			
		}
		/* NO FLOW */
		else {
			String noFlowStatistic = null;
			Alarm noFlowAlarm = null;
			
			EquinoxEvent e = EquinoxEvent.getEquinoxEventFrom( r.getRet() );
			
			SIPResponseCode noFlowResponseCode = null;
			
			switch ( e ) {
			case ERROR: break;
				
			case REJECT: break;
				
			case ABORT: break;
			
			case TIMEOUT:
				
				System.out.println( "Extend Timeout For ESTABLISHED State." );
				
				this.ec02Instance.setTimeout( "60" );

				this.nextState = this.currentState;
				
				break;
				
			default:
				break;
			}
			
			// IO DETAIL - NO FLOW INPUT DETAIL
			InputDetail noFlowInputDetail = this.detail.new InputDetail(
					r.getInvoke(),
					new AppDetailEvent( "App", "Event" ),
					r.getType(),
					r.getRawDataAttributes(),
					r.getRawDataAttribute( "val" ),
					transactionStartTime - this.appInstance.getLastInstanceUpdatedTime()
					);
			
			noFlowInputDetail.setData( (ServerAssignmentMessage) JAXBHandler.createInstance(
					InstanceContext.getServerAssignmentContext(),
					r.getRawDataMessage(),
					ServerAssignmentMessage.class )
					);
			
			this.detail.getTransactionDetail().addInput( noFlowInputDetail );
			this.detail.getTransactionDetail().setInputTimeStamp( transactionStartTime );
			
			this.ec02Instance.incrementsStat( noFlowStatistic );
			this.ec02Instance.raiseAlarm( noFlowAlarm, new String[]{} );
			
			// GENERATE SIP MESSAGE
			SIPResponse noFlowResponse = new SIPResponse( noFlowResponseCode );
			noFlowResponse.setHeaders( this.appInstance.getSipMessage().getHeaders() );
			noFlowResponse.setContent( this.appInstance.getSipMessage().getContent() );
			
			HashMap<String, String> attrs = new HashMap<String, String>();
			attrs.put( "name", "SOCKET" );
			attrs.put( "ctype", "udp" );
			attrs.put( "type", MessageType.RESPONSE );
			attrs.put( "to", this.appInstance.getOrigin() );
			attrs.put( "val", noFlowResponse.toString() );
			
			EquinoxRawData output = new EquinoxRawData();
			output.setInvoke( this.appInstance.getOriginInvoke() );
			output.setRawDataAttributes( attrs );
			
			this.outputs.add( output );
			
			this.appInstance.clearOrigin();
			this.appInstance.clearOriginInvoke();
			
			this.nextState = States.ACTIVE;
			
			// UDR
			
			// IODETAL - NO FLOW OUTPUT DETAIL
			OutputDetail noFlowOutputDetail = this.detail.new OutputDetail(
					output.getInvoke(),
					new AppDetailEvent( "App", "Event" ),
					output.getType(),
					output.getRawDataAttributes(),
					output.getRawDataAttribute( "val" )
					);
			
			noFlowOutputDetail.setData( noFlowResponse );
			
			this.detail.getTransactionDetail().addOutput( noFlowOutputDetail );
		}
		
		/* */
		long transactionEndTime = System.currentTimeMillis();
		
		this.appInstance.setLastInstanceUpdatedTime( transactionEndTime );
		
		this.appInstance.clearOrigin();
		this.appInstance.clearOriginInvoke();
		
		this.ec02Instance.setEquinoxRawDatas( this.outputs );
		this.ec02Instance.setRet( RetNumber.NORMAL );
		
		this.detail.getTransactionDetail().setOutputTimeStamp( transactionEndTime );
		
		this.detail.setNextState( this.nextState );
		this.detail.setProcessingTime( transactionEndTime - transactionStartTime );
		
		System.out.println( "---" );
		System.out.println(
				new GsonBuilder().setPrettyPrinting().disableHtmlEscaping().create().toJson( this.detail )
				);
		
		return this.nextState;
			
		}
		
/*		 
		long transactionEndTime = System.currentTimeMillis();
		
		this.appInstance.setLastInstanceUpdatedTime( transactionEndTime );
		this.appInstance.setOutgoingTimeout( Integer.parseInt( this.ec02Instance.getTimeout() ) );
		
		this.ec02Instance.setEquinoxRawDatas( this.outputs );
		this.ec02Instance.setRet( RetNumber.NORMAL );
		
		this.detail.getTransactionDetail().setOutputTimeStamp( transactionEndTime );
		
		this.detail.setNextState( this.nextState );
		this.detail.setProcessingTime( transactionEndTime - transactionStartTime );
		
		System.out.println( "---" );
		System.out.println(
				new GsonBuilder().setPrettyPrinting().disableHtmlEscaping().create().toJson( this.detail )
				);
		
		return this.nextState;
*/

}
