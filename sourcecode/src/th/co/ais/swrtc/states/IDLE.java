
package th.co.ais.swrtc.states;

import java.util.ArrayList;
import java.util.HashMap;

import th.co.ais.swrtc.enums.SIPRequestMethod;
import th.co.ais.swrtc.exception.SIPParseException;
import th.co.ais.swrtc.exception.VerificationFailedException;
import th.co.ais.swrtc.instances.APPInstance;
import th.co.ais.swrtc.instances.EC02Instance;
import th.co.ais.swrtc.instances.MultimediaAuthMessage;
import th.co.ais.swrtc.instances.SIPMessage;
import th.co.ais.swrtc.interfaces.APPConfig;
import th.co.ais.swrtc.interfaces.MessageType;
import th.co.ais.swrtc.interfaces.RetNumber;
import th.co.ais.swrtc.interfaces.States;
import th.co.ais.swrtc.interfaces.Statistic;
import th.co.ais.swrtc.interfaces.TerminationCode;
import th.co.ais.swrtc.jaxb.InstanceContext;
import th.co.ais.swrtc.jaxb.JAXBHandler;
import th.co.ais.swrtc.utils.AppDetail;
import th.co.ais.swrtc.utils.AppDetail.InputDetail;
import th.co.ais.swrtc.utils.AppDetail.OutputDetail;
import th.co.ais.swrtc.utils.AppDetailEvent;
import th.co.ais.swrtc.utils.ConfigureTool;
import th.co.ais.swrtc.utils.IDGenerator;
import th.co.ais.swrtc.utils.SIPParser;
import th.co.ais.swrtc.verificator.SIPVerificator;

import com.google.gson.GsonBuilder;

import ec02.af.abstracts.AbstractAF;
import ec02.af.data.EquinoxRawData;
import ec02.af.interfaces.IAFState;

public class IDLE implements IAFState{
	
	private ArrayList<EquinoxRawData> outputs;
	
	private EC02Instance ec02Instance;
	
	private APPInstance appInstance;
	private AppDetail detail;
	
	private String nextState;
	
	@Override
	public String doAction( AbstractAF af, Object instance, ArrayList<EquinoxRawData> rawDatas ) {
		
		long transactionStartTime = System.currentTimeMillis();
		
		this.ec02Instance = ( EC02Instance ) instance;
		this.appInstance = this.ec02Instance.getAppInstance();
		
		this.outputs = new ArrayList<EquinoxRawData>();
		
		this.detail = new AppDetail();
		this.detail.setCurrentState( this.ec02Instance.getProperties().getState() );
		this.detail.getTransactionDetail().setSession( "equinox-session" );
		this.detail.getTransactionDetail().setInitialInvoke( "initial-invoke" );
		
		EquinoxRawData r = rawDatas.get( 0 );
		
		String normalRawDataMessage = r.getRawDataCDATAAttributes( "val" );

		try {
			
			this.appInstance.setOrigin( r.getOrig() );
			this.appInstance.setOriginInvoke( r.getInvoke() );
			
			// IODETAIL - INCOMING INITIAL REGISTER MESSAGE
			InputDetail normalInputDetail = this.detail.new InputDetail(
					r.getInvoke(),
					new AppDetailEvent( r.getOrig(), "Test-Command" ),
					r.getType(),
					r.getRawDataAttributes(),
					normalRawDataMessage, null
					);
			
			SIPMessage sip = SIPParser.getMessage( normalRawDataMessage );
			
			// VERIFY 'REGISTER' MESSAGE
			SIPVerificator.verify( SIPRequestMethod.INVITE, sip );
			
			normalInputDetail.setData( sip );
			
			this.detail.getTransactionDetail().addInput( normalInputDetail );
			this.detail.getTransactionDetail().setInputTimeStamp( transactionStartTime );
			
			this.appInstance.setSIPMessage( sip );
			
			this.ec02Instance.incrementsStat( Statistic.APP_RECEIVE_UNAUTHEN_REGISTER );
			
			// 'MAR' TO MAAA
			HashMap<String, String> attrs = new HashMap<String, String>();
			attrs.put( "name", "DIAMETER" );
			attrs.put( "ctype", "Credit-Control" );
			attrs.put( "type", MessageType.REQUEST );
			attrs.put( "to", ConfigureTool.getConfigure( APPConfig.MAAA_INTERFACE ) );
			
			MultimediaAuthMessage mar = new MultimediaAuthMessage(
					"ec2-54-197-180-128.compute-1.amazonaws.com;1396640817;5", 
					null, "1", "IWRTC-5-2-0.3SUK4NS.ais.co.th",
					"sand.ais.co.th", "AAA-1-0-0.3SUK4NS.ais.co.th", "toro.ais.co.th", 
					null, null, null, null, null, null, null, null, null );
			
			EquinoxRawData output = new EquinoxRawData();
			output.setInvoke( IDGenerator.generateUniqueIdWithPrefix( "MAR" ) );
			output.setRawMessage( 
					JAXBHandler.composeMessage( InstanceContext.getMultimediaAuthContext(), mar )
					.replaceAll( "</*MultimediaAuthen>", "" ).trim()
					);
			output.setRawDataAttributes( attrs );
			
			this.outputs.add( output );
			
			this.appInstance.addPendingRequest( output.getInvoke() );
			this.appInstance.getAppTermination().setTerminationOfIdleState( TerminationCode.SUCCESS );
			
			this.ec02Instance.setTimeout( ConfigureTool.getConfigure( APPConfig.RESPONSE_TIME_LIMIT ) );
			
			this.ec02Instance.incrementsStat( Statistic.APP_REQUEST_MAR_TO_MAAA );
			
			this.nextState = States.WAIT_MULTIMEDIA_AUTH_ANSWER;
			
			// IO DETAIL - OUTGONING 'MAR'
			OutputDetail outputDetail = this.detail.new OutputDetail(
					output.getInvoke(),
					new AppDetailEvent( "App", "Command" ),
					output.getType(),
					output.getRawDataAttributes(),
					output.getRawDataMessage()
					);
			
			outputDetail.setData( null );
			
			this.detail.getTransactionDetail().addOutput( outputDetail );
			
		}
		catch ( SIPParseException e ) {
			
			HashMap<String, String> attrs = new HashMap<String, String>();
			attrs.put( "name", "SOCKET" );
			attrs.put( "ctype", "udp" );
			attrs.put( "type", MessageType.RESPONSE );
			attrs.put( "to", this.appInstance.getOrigin() );
			attrs.put( "val", " SIP PARSE EXCEPTION " );
			
			EquinoxRawData output = new EquinoxRawData();
			output.setInvoke( this.appInstance.getOriginInvoke() );
			output.setRawDataAttributes( attrs );
			
			this.appInstance.clearOrigin();
			this.appInstance.clearOriginInvoke();
			
			this.nextState = States.IDLE;
			
			this.ec02Instance.incrementsStat( e.getStatistic() );
			this.ec02Instance.raiseAlarm( e.getAlarm(), new String[] {} );
			
		}
		catch ( VerificationFailedException e ) {
			
			HashMap<String, String> attrs = new HashMap<String, String>();
			attrs.put( "name", "SOCKET" );
			attrs.put( "ctype", "udp" );
			attrs.put( "type", MessageType.RESPONSE );
			attrs.put( "to", this.appInstance.getOrigin() );
			attrs.put( "val", " VERIFICATION FAILED EXCEPTION " );
			
			EquinoxRawData output = new EquinoxRawData();
			output.setInvoke( this.appInstance.getOriginInvoke() );
			output.setRawDataAttributes( attrs );
			
			this.appInstance.clearOrigin();
			this.appInstance.clearOriginInvoke();
			
			this.nextState = States.IDLE;
			
			this.ec02Instance.incrementsStat( e.getStatistic() );
			this.ec02Instance.raiseAlarm( e.getAlarm(), new String[] {} );
			
		}
		
		/* */
		long transactionEndTime = System.currentTimeMillis();
		
		this.appInstance.setLastInstanceUpdatedTime( transactionEndTime );
		
		this.ec02Instance.setEquinoxRawDatas( this.outputs );
		this.ec02Instance.setRet( RetNumber.NORMAL );
		
		this.detail.getTransactionDetail().setOutputTimeStamp( transactionEndTime );
		
		this.detail.setNextState( this.nextState );
		this.detail.setProcessingTime( transactionEndTime - transactionStartTime );
		
		System.out.println( "---" );
		System.out.println(
				new GsonBuilder().setPrettyPrinting().disableHtmlEscaping().create().toJson( this.detail )
				);
		
		return this.nextState;
	}

}