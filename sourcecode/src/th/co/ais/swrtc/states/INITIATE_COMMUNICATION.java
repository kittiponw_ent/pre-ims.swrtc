package th.co.ais.swrtc.states;

import java.util.ArrayList;
import java.util.HashMap;

import th.co.ais.swrtc.enums.Alarm;
import th.co.ais.swrtc.enums.EquinoxEvent;
import th.co.ais.swrtc.enums.SIPRequestMethod;
import th.co.ais.swrtc.enums.SIPResponseCode;
import th.co.ais.swrtc.exception.SIPParseException;
import th.co.ais.swrtc.exception.VerificationFailedException;
import th.co.ais.swrtc.instances.APPInstance;
import th.co.ais.swrtc.instances.EC02Instance;
import th.co.ais.swrtc.instances.SIPMessage;
import th.co.ais.swrtc.instances.SIPRequest;
import th.co.ais.swrtc.instances.SIPResponse;
import th.co.ais.swrtc.instances.ServerAssignmentMessage;
import th.co.ais.swrtc.interfaces.APPConfig;
import th.co.ais.swrtc.interfaces.ActionType;
import th.co.ais.swrtc.interfaces.Event;
import th.co.ais.swrtc.interfaces.MessageType;
import th.co.ais.swrtc.interfaces.RetNumber;
import th.co.ais.swrtc.interfaces.States;
import th.co.ais.swrtc.jaxb.InstanceContext;
import th.co.ais.swrtc.jaxb.JAXBHandler;
import th.co.ais.swrtc.utils.AppDetail;
import th.co.ais.swrtc.utils.AppDetail.InputDetail;
import th.co.ais.swrtc.utils.AppDetail.OutputDetail;
import th.co.ais.swrtc.utils.AppDetailEvent;
import th.co.ais.swrtc.utils.ConfigureTool;
import th.co.ais.swrtc.utils.IDGenerator;
import th.co.ais.swrtc.utils.SIPHeaderHandler;
import th.co.ais.swrtc.utils.SIPParser;
import th.co.ais.swrtc.verificator.SIPVerificator;

import com.google.gson.GsonBuilder;

import ec02.af.abstracts.AbstractAF;
import ec02.af.data.EquinoxRawData;
import ec02.af.interfaces.IAFState;

public class INITIATE_COMMUNICATION implements IAFState {
	
	private ArrayList<EquinoxRawData> outputs;
	
	private EC02Instance ec02Instance;
	
	private APPInstance appInstance;
	private AppDetail detail;
	
	private String currentState;
	private String nextState;

	@Override
	public String doAction( AbstractAF af, Object instance, ArrayList<EquinoxRawData> rawDatas ) {
		
		long transactionStartTime = System.currentTimeMillis();
		
		this.ec02Instance = ( EC02Instance ) instance;
		this.appInstance = this.ec02Instance.getAppInstance();
		
		this.currentState = this.ec02Instance.getProperties().getState();
		
		this.outputs = new ArrayList<EquinoxRawData>();
		
		this.detail = new AppDetail();
		this.detail.setCurrentState( this.ec02Instance.getProperties().getState() );
		this.detail.getTransactionDetail().setSession( "equinox-session" );
		this.detail.getTransactionDetail().setInitialInvoke( "initial-invoke" );
		
		EquinoxRawData r = rawDatas.get( 0 );
		
		String pendingRequest = this.appInstance.getPendingRequests().get( 0 );
		
		/* NORMAL FLOW */
		if ( RetNumber.NORMAL.equals( r.getRet() ) ) {
			
			if ( ! r.getInvoke().equals( pendingRequest ) ) {
				return currentState;
			}
			
			this.appInstance.completePendingRequest( pendingRequest );
			
			String normalRawDataMessage = r.getRawDataCDATAAttributes( "val" );
			
			try {
				
				// IODETAIL
				InputDetail normalInputDetail = this.detail.new InputDetail(
						r.getInvoke(),
						new AppDetailEvent( r.getOrig(), "Test-Command" ),
						r.getType(),
						r.getRawDataAttributes(),
						normalRawDataMessage, null
						);
				
				SIPMessage sip = SIPParser.getMessage( normalRawDataMessage );
				
				normalInputDetail.setData( sip );
				
				this.detail.getTransactionDetail().addInput( normalInputDetail );
				this.detail.getTransactionDetail().setInputTimeStamp( transactionStartTime );
				
				// SIP RESPONSE
				if ( ( sip instanceof SIPResponse ) 
						&& ( Event.WAIT_200_FROM_AS.equals( this.appInstance.getEvent() ) ) ) {
					
					// VERIFY INCOMING '200' MESSAGE
					SIPVerificator.verify( SIPResponseCode.OK, sip );
					
					String ownRecRouteAddress = "initiate communication as.address";
					String ownViaAddress = "SIP/2.0/UDP initiate communication as.address";
					
					SIPHeaderHandler.removeRoute( sip );
					SIPHeaderHandler.addRecordRoute( sip, ownRecRouteAddress );
					SIPHeaderHandler.addVia( sip, ownViaAddress );
					
					// FORWARD '200' MESSAGE
					
					HashMap<String, String> okAttrs = new HashMap<String, String>();
					okAttrs.put( "name", "SOCKET" );
					okAttrs.put( "ctype", "udp" );
					okAttrs.put( "type", MessageType.REQUEST );
					okAttrs.put( "val", sip.toString() );
					okAttrs.put( "to", 
							ActionType.CALLER.equals( this.appInstance.getActionType() )
								? ConfigureTool.getConfigure( APPConfig.PWRTC_INTERFACE )
								: ConfigureTool.getConfigure( APPConfig.IWRTC_INTERFACE )
							);
					
					EquinoxRawData okOutput = new EquinoxRawData();
					okOutput.setInvoke( IDGenerator.generateUniqueId() );
					okOutput.setRawDataAttributes( okAttrs );
					
					this.outputs.add( okOutput );
					
					this.appInstance.addPendingRequest( okOutput.getInvoke() );
					this.appInstance.setEvent(
							ActionType.CALLER.equals( this.appInstance.getActionType() )
								? Event.WAIT_ACK_FROM_P
								: Event.WAIT_ACK_FROM_I
							);
					
					this.nextState = this.currentState;
				
				} 
				// SIP REQUEST
				else if ( sip instanceof SIPRequest ) {
					
					// VERIFY INCOMING 'ACK' MESSAGE
					SIPVerificator.verify( SIPRequestMethod.ACK, sip );
					
					if ( Event.WAIT_ACK_FROM_P.equals( this.appInstance.getEvent() ) 
							|| Event.WAIT_ACK_FROM_I.equals( this.appInstance.getEvent() ) ) {
					
						String ownRecRouteAddress = "initiate communication ack_pwrtc.address";
						String ownViaAddress = "SIP/2.0/UDP initiate communication ack_pwrtc.address";
						
						SIPHeaderHandler.removeRoute( sip );
						SIPHeaderHandler.addRecordRoute( sip, ownRecRouteAddress );
						SIPHeaderHandler.addVia( sip, ownViaAddress );
						
						// FORWARD 'ACK'
						HashMap<String, String> ackAttrs = new HashMap<String, String>();
						ackAttrs.put( "name", "SOCKET" );
						ackAttrs.put( "ctype",  "udp" );
						ackAttrs.put( "type", MessageType.REQUEST );
						ackAttrs.put( "val" , sip.toString() );
						ackAttrs.put( "to", ConfigureTool.getConfigure( APPConfig.AS_INTERFACE ) );
						
						EquinoxRawData ackOutput = new EquinoxRawData();
						ackOutput.setInvoke( IDGenerator.generateUniqueId() );
						ackOutput.setRawDataAttributes( ackAttrs );
						
						this.outputs.add( ackOutput );
						
						this.appInstance.addPendingRequest( ackOutput.getInvoke() );
						this.appInstance.setEvent( Event.WAIT_ACK_FROM_AS );
						
						this.nextState = this.currentState;
						
					}
					else if ( Event.WAIT_ACK_FROM_AS.equals( this.appInstance.getEvent() ) ) {
						
						String ownRecordRouteAddress = "initiate communication ack_as.address";
						String ownViaAddress = "SIP/2.0/UDP initiate communication ack_as.address";
						
						String ownRouteAddress = "initiate communication ack as.address";
						String nextRouteAddress = "initiate communication ack as to i.address";
						
						SIPHeaderHandler.addRoute( sip, ownRouteAddress, nextRouteAddress );
						SIPHeaderHandler.addRecordRoute( sip, ownRecordRouteAddress );
						SIPHeaderHandler.addVia( sip, ownViaAddress );
						
						// RESPONSE BACK TO IWRTC ( CALLER ), PWRTC ( CALLEE )
						HashMap<String, String> ackAttrs = new HashMap<String, String>();
						ackAttrs.put( "name", "SOCKET" );
						ackAttrs.put( "ctype",  "udp" );
						ackAttrs.put( "type", MessageType.RESPONSE );
						ackAttrs.put( "to", this.appInstance.getOrigin() );
						ackAttrs.put( "val" , sip.toString() );
						
						EquinoxRawData output = new EquinoxRawData();
						output.setInvoke( this.appInstance.getOriginInvoke() );
						output.setRawDataAttributes( ackAttrs );
						
						this.outputs.add( output );
						
						this.appInstance.clearEvent();
						this.appInstance.clearOrigin();
						this.appInstance.clearOriginInvoke();
						
						this.ec02Instance.setTimeout( ConfigureTool.getConfigure( APPConfig.RESPONSE_TIME_LIMIT ) );
						
						this.nextState = States.ESTABLISHED;
						
					}
						
				}
				
			} catch ( SIPParseException e ) {

				HashMap<String, String> attrs = new HashMap<String, String>();
				attrs.put( "name", "SOCKET" );
				attrs.put( "ctype", "udp" );
				attrs.put( "type", MessageType.RESPONSE );
				attrs.put( "to", this.appInstance.getOrigin() );
				attrs.put( "val", " SIP PARSE EXCEPTION " );
				
				EquinoxRawData output = new EquinoxRawData();
				output.setInvoke( this.appInstance.getOriginInvoke() );
				output.setRawDataAttributes( attrs );
				
				this.appInstance.clearOrigin();
				this.appInstance.clearOriginInvoke();
				
				this.appInstance.updateLifeSpan();
				
				this.ec02Instance.setTimeout( 
						Long.toString( this.appInstance.getLifeSpan() )
						);
				
				this.nextState = States.ACTIVE;
				
			} catch (VerificationFailedException e) {

				HashMap<String, String> attrs = new HashMap<String, String>();
				attrs.put( "name", "SOCKET" );
				attrs.put( "ctype", "udp" );
				attrs.put( "type", MessageType.RESPONSE );
				attrs.put( "to", this.appInstance.getOrigin() );
				attrs.put( "val", " INVITE VERIFICATION FAILED EXCEPTION " );
				
				EquinoxRawData output = new EquinoxRawData();
				output.setInvoke( this.appInstance.getOriginInvoke() );
				output.setRawDataAttributes( attrs );
				
				this.appInstance.clearOrigin();
				this.appInstance.clearOriginInvoke();
				
				this.appInstance.updateLifeSpan();
				
				this.ec02Instance.setTimeout( 
						Long.toString( this.appInstance.getLifeSpan() )
						);
				
				this.nextState = States.ACTIVE;
				e.printStackTrace();
				
			}
			
		} 
		/* NO FLOW */
		else {
			String noFlowStatistic = null;
			Alarm noFlowAlarm = null;
			
			EquinoxEvent e = EquinoxEvent.getEquinoxEventFrom( r.getRet() );
			
			SIPResponseCode noFlowResponseCode = null;
			
			switch ( e ) {
			case ERROR:
				noFlowStatistic				= null;
				noFlowAlarm					= null;
				noFlowResponseCode			= SIPResponseCode.SERVER_INTERNAL_ERROR;
				break;
				
			case REJECT:
				noFlowStatistic				= null;
				noFlowAlarm					= null;
				noFlowResponseCode			= SIPResponseCode.SERVER_INTERNAL_ERROR;
				break;
				
			case ABORT:
				noFlowStatistic				= null;
				noFlowAlarm					= null;
				noFlowResponseCode			= SIPResponseCode.SERVER_INTERNAL_ERROR;
				break;
				
			case TIMEOUT:
				noFlowStatistic				= null;
				noFlowAlarm					= null;
				noFlowResponseCode			= SIPResponseCode.SERVER_INTERNAL_ERROR;
				break;
				
			default:
				break;
			}
			
			// IO DETAIL - NO FLOW INPUT DETAIL
			InputDetail noFlowInputDetail = this.detail.new InputDetail(
					r.getInvoke(),
					new AppDetailEvent( "App", "Event" ),
					r.getType(),
					r.getRawDataAttributes(),
					r.getRawDataAttribute( "val" ),
					transactionStartTime - this.appInstance.getLastInstanceUpdatedTime()
					);
			
			noFlowInputDetail.setData( (ServerAssignmentMessage) JAXBHandler.createInstance(
					InstanceContext.getServerAssignmentContext(),
					r.getRawDataMessage(),
					ServerAssignmentMessage.class )
					);
			
			this.detail.getTransactionDetail().addInput( noFlowInputDetail );
			this.detail.getTransactionDetail().setInputTimeStamp( transactionStartTime );
			
			this.ec02Instance.incrementsStat( noFlowStatistic );
			this.ec02Instance.raiseAlarm( noFlowAlarm, new String[]{} );
			
			// GENERATE SIP MESSAGE
			SIPResponse noFlowResponse = new SIPResponse( noFlowResponseCode );
			noFlowResponse.setHeaders( this.appInstance.getSipMessage().getHeaders() );
			noFlowResponse.setContent( this.appInstance.getSipMessage().getContent() );
			
			HashMap<String, String> attrs = new HashMap<String, String>();
			attrs.put( "name", "SOCKET" );
			attrs.put( "ctype", "udp" );
			attrs.put( "type", MessageType.RESPONSE );
			attrs.put( "to", this.appInstance.getOrigin() );
			attrs.put( "val", noFlowResponse.toString() );
			
			EquinoxRawData output = new EquinoxRawData();
			output.setInvoke( this.appInstance.getOriginInvoke() );
			output.setRawDataAttributes( attrs );
			
			this.outputs.add( output );
			
			this.appInstance.clearOrigin();
			this.appInstance.clearOriginInvoke();
			
			this.nextState = States.ACTIVE;
			
			// UDR
			
			// IODETAL - NO FLOW OUTPUT DETAIL
			OutputDetail noFlowOutputDetail = this.detail.new OutputDetail(
					output.getInvoke(),
					new AppDetailEvent( "App", "Event" ),
					output.getType(),
					output.getRawDataAttributes(),
					output.getRawDataAttribute( "val" )
					);
			
			noFlowOutputDetail.setData( noFlowResponse );
			
			this.detail.getTransactionDetail().addOutput( noFlowOutputDetail );
		}
		
		/* */
		long transactionEndTime = System.currentTimeMillis();
		
		this.appInstance.setLastInstanceUpdatedTime( transactionEndTime );
		
		this.ec02Instance.setEquinoxRawDatas( this.outputs );
		this.ec02Instance.setRet( RetNumber.NORMAL );
		
		this.detail.getTransactionDetail().setOutputTimeStamp( transactionEndTime );
		
		this.detail.setNextState( this.nextState );
		this.detail.setProcessingTime( transactionEndTime - transactionStartTime );
		
		System.out.println( "---" );
		System.out.println(
				new GsonBuilder().setPrettyPrinting().disableHtmlEscaping().create().toJson( this.detail )
				);
		
		return this.nextState;
	}

}
