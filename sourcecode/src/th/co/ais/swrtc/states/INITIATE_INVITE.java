package th.co.ais.swrtc.states;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import th.co.ais.swrtc.enums.Alarm;
import th.co.ais.swrtc.enums.EquinoxEvent;
import th.co.ais.swrtc.enums.SIPRequestMethod;
import th.co.ais.swrtc.enums.SIPResponseCode;
import th.co.ais.swrtc.exception.SIPParseException;
import th.co.ais.swrtc.exception.VerificationFailedException;
import th.co.ais.swrtc.instances.APPInstance;
import th.co.ais.swrtc.instances.EC02Instance;
import th.co.ais.swrtc.instances.SIPMessage;
import th.co.ais.swrtc.instances.SIPRequest;
import th.co.ais.swrtc.instances.SIPResponse;
import th.co.ais.swrtc.instances.ServerAssignmentMessage;
import th.co.ais.swrtc.interfaces.APPConfig;
import th.co.ais.swrtc.interfaces.ActionType;
import th.co.ais.swrtc.interfaces.Event;
import th.co.ais.swrtc.interfaces.MessageType;
import th.co.ais.swrtc.interfaces.RetNumber;
import th.co.ais.swrtc.interfaces.States;
import th.co.ais.swrtc.jaxb.InstanceContext;
import th.co.ais.swrtc.jaxb.JAXBHandler;
import th.co.ais.swrtc.utils.AppDetail;
import th.co.ais.swrtc.utils.AppDetail.InputDetail;
import th.co.ais.swrtc.utils.AppDetail.OutputDetail;
import th.co.ais.swrtc.utils.AppDetailEvent;
import th.co.ais.swrtc.utils.ConfigureTool;
import th.co.ais.swrtc.utils.IDGenerator;
import th.co.ais.swrtc.utils.SIPHeaderHandler;
import th.co.ais.swrtc.utils.SIPParser;
import th.co.ais.swrtc.verificator.SIPVerificator;

import com.google.gson.GsonBuilder;

import ec02.af.abstracts.AbstractAF;
import ec02.af.data.EquinoxRawData;
import ec02.af.interfaces.IAFState;

public class INITIATE_INVITE implements IAFState {
	
	private ArrayList<EquinoxRawData> outputs;
	
	private EC02Instance ec02Instance;
	
	private APPInstance appInstance;
	private AppDetail detail;
	
	private String currentState;
	private String nextState;

	@Override
	public String doAction( AbstractAF af, Object instance, ArrayList<EquinoxRawData> rawDatas ) {
		
		long transactionStartTime = System.currentTimeMillis();
		
		this.ec02Instance = ( EC02Instance ) instance;
		this.appInstance = this.ec02Instance.getAppInstance();
		
		this.outputs = new ArrayList<EquinoxRawData>();
		
		this.currentState = this.ec02Instance.getProperties().getState();
		
		this.detail = new AppDetail();
		this.detail.setCurrentState( this.ec02Instance.getProperties().getState() );
		this.detail.getTransactionDetail().setSession( "equinox-session" );
		this.detail.getTransactionDetail().setInitialInvoke( "initial-invoke" );
		
		EquinoxRawData r = rawDatas.get( 0 );
		
		/* NORMAL FLOW */
		if ( RetNumber.NORMAL.equals( r.getRet() ) ) {
			
			String normalRawDataMessage = r.getRawDataCDATAAttributes( "val" );
			
			try {
				
				// IODETAIL
				InputDetail normalInputDetail = this.detail.new InputDetail(
						r.getInvoke(),
						new AppDetailEvent( r.getOrig(), "Test-Command" ),
						r.getType(),
						r.getRawDataAttributes(),
						normalRawDataMessage, null
						);
				
				SIPMessage sip = SIPParser.getMessage( normalRawDataMessage );
				
				normalInputDetail.setData( sip );
				
				this.detail.getTransactionDetail().addInput( normalInputDetail );
				this.detail.getTransactionDetail().setInputTimeStamp( transactionStartTime );
				
				// SIP REQUEST
				if ( ( sip instanceof SIPRequest ) 
						&& ( Event.WAIT_INVITE_FROM_AS.equals( this.appInstance.getEvent() ) ) ) {
					
					String pendingRequest = this.appInstance.getPendingRequests().get( 0 );
					
					if ( ! pendingRequest.equals( r.getInvoke() ) ) {
						return currentState;
						
					}
					
					this.appInstance.completePendingRequest( pendingRequest );
					
					// VERIFY INCOMING 'INVITE' MESSAGE
					SIPVerificator.verify( SIPRequestMethod.INVITE, sip );
					
					String ownRecordRouteAddress = "originate-as.address";
					String ownViaAddress = "SIP/2.0/UDP originate-as";
					
					SIPHeaderHandler.removeRoute( sip );
					SIPHeaderHandler.addRecordRoute( sip, ownRecordRouteAddress );
					SIPHeaderHandler.addVia( sip, ownViaAddress );
					
					// CREATE 'INVITE'
					SIPRequest invitation = new SIPRequest( SIPRequestMethod.INVITE, "<abc@123.1.2>" );
					invitation.setHeaders( sip.getHeaders() );
					invitation.setContent( sip.getContent() );
					
					HashMap<String, String> invitationAttrs = new HashMap<String, String>();
					invitationAttrs.put( "name", "SOCKET" );
					invitationAttrs.put( "ctype", "udp" );
					invitationAttrs.put( "type", MessageType.REQUEST );
					invitationAttrs.put( "val", invitation.toString() );
					
					EquinoxRawData invitationOutput = new EquinoxRawData();
					invitationOutput.setInvoke( IDGenerator.generateUniqueId() );

					// CREATE '100'
					SIPResponse trying = new SIPResponse( SIPResponseCode.TRYING );
					trying.setHeaders( sip.getHeaders() );
					trying.setContent( sip.getContent() );
					
					HashMap<String, String> tryingAttrs = new HashMap<String, String>();
					tryingAttrs.put( "name", "SOCKET" );
					tryingAttrs.put( "ctype", "udp" );
					tryingAttrs.put( "type", MessageType.REQUEST );
					tryingAttrs.put( "val", trying.toString() );
					
					EquinoxRawData tryingOutput = new EquinoxRawData();
					tryingOutput.setInvoke( IDGenerator.generateUniqueId() );
					
					if ( ActionType.CALLER.equals( this.appInstance.getActionType() ) ) {
						invitationAttrs.put( "to", ConfigureTool.getConfigure( APPConfig.IWRTC_INTERFACE ) );
						tryingAttrs.put( "to", ConfigureTool.getConfigure( APPConfig.PWRTC_INTERFACE ) );
						
					}
					else {
						invitationAttrs.put( "to", ConfigureTool.getConfigure( APPConfig.PWRTC_INTERFACE ) );
						tryingAttrs.put( "to", ConfigureTool.getConfigure( APPConfig.IWRTC_INTERFACE ) );
						
					}
					
					invitationOutput.setRawDataAttributes( invitationAttrs );
					this.outputs.add( invitationOutput );
					
					tryingOutput.setRawDataAttributes( tryingAttrs );
					this.outputs.add( tryingOutput );
					
					this.appInstance.addPendingRequest( invitationOutput.getInvoke() );
					this.appInstance.clearEvent();
					
					this.nextState = this.currentState;
					
				}
				// SIP RESPONSE
				else if ( sip instanceof SIPResponse ) {
					
					// VERIFY INCOMING '183' OR '100' MESSAGE
					SIPVerificator.verify( 
							Arrays.asList( SIPResponseCode.RINGING, SIPResponseCode.TRYING ), sip 
							);
					
					SIPResponseCode responseCode = SIPResponseCode.getResponseCode( ( ( SIPResponse ) sip ).getCode() );
					
					switch ( responseCode ) {
					
					// '183'
					case SESSION_IN_PROGRESS:
						
						String pendingRequest = this.appInstance.getPendingRequests().get( 0 );
						
						if ( ! pendingRequest.equals( r.getInvoke() ) ) {
							return currentState;
							
						}
						
						this.appInstance.completePendingRequest( pendingRequest );
						
						// CREATE '183'
						SIPResponse sessionInProgress = new SIPResponse( SIPResponseCode.SESSION_IN_PROGRESS );
						sessionInProgress.setHeaders( sip.getHeaders() );
						sessionInProgress.setContent( sip.getContent() );
						
						HashMap<String, String> sessionInProgressAttrs = new HashMap<String, String>();
						sessionInProgressAttrs.put( "name", "SOCKET" );
						sessionInProgressAttrs.put( "ctype", "udp" );
						sessionInProgressAttrs.put( "type", MessageType.RESPONSE );
						sessionInProgressAttrs.put( "val", sessionInProgress.toString() );
						sessionInProgressAttrs.put( "to", this.appInstance.getOrigin() );
						
						EquinoxRawData sessionInProgressOutput = new EquinoxRawData();
						sessionInProgressOutput.setInvoke( this.appInstance.getOriginInvoke() );
						sessionInProgressOutput.setRawDataAttributes( sessionInProgressAttrs );
						
						this.outputs.add( sessionInProgressOutput );
						
						this.appInstance.clearOrigin();
						this.appInstance.clearOriginInvoke();
						
						this.nextState = States.WAIT_RINGING;
						
						break;

					// '100'
					case TRYING:
						// INCREMENT A STATISTIC
						this.nextState = this.currentState;
						
						break;

					default:
						break;
					
					}
					
				}
				// CRITICAL ERROR
				else {
					// DO SOMETHING
					// STILL WAITING IN THIS STATE
					this.nextState = this.currentState;
				}
				
			} catch ( SIPParseException e ) {
				
				/*
				 * IF EQUINOX TYPE IS 'REQUEST', ASSUME '100 TRYING' PARSE ERROR
				 * 		- STILL WAITING IN THIS STATE
				 * 
				 * IF EVENT IS 'WAIT_INVITE_FROM_AS'
				 * 		- 
				 */
				
				HashMap<String, String> attrs = new HashMap<String, String>();
				attrs.put( "name", "SOCKET" );
				attrs.put( "ctype", "udp" );
				attrs.put( "type", MessageType.RESPONSE );
				attrs.put( "to", this.appInstance.getOrigin() );
				attrs.put( "val", " SIP PARSE EXCEPTION " );
				
				EquinoxRawData output = new EquinoxRawData();
				output.setInvoke( this.appInstance.getOriginInvoke() );
				output.setRawDataAttributes( attrs );
				
				this.appInstance.clearOrigin();
				this.appInstance.clearOriginInvoke();
				
				this.appInstance.updateLifeSpan();
				
				this.ec02Instance.setTimeout( 
						Long.toString( this.appInstance.getLifeSpan() )
						);
				
				this.nextState = States.ACTIVE;
				
			} catch ( VerificationFailedException e ) {
				
				/*
				 * IF EQUINOX TYPE IS 'REQUEST', ASSUME '100 TRYING' PARSE ERROR
				 * 		- STILL WAITING IN THIS STATE
				 * 
				 * IF EVENT IS 'WAIT_INVITE_FROM_AS'
				 * 		- 
				 */

				HashMap<String, String> attrs = new HashMap<String, String>();
				attrs.put( "name", "SOCKET" );
				attrs.put( "ctype", "udp" );
				attrs.put( "type", MessageType.RESPONSE );
				attrs.put( "to", this.appInstance.getOrigin() );
				attrs.put( "val", " INVITE VERIFICATION FAILED EXCEPTION " );
				
				EquinoxRawData output = new EquinoxRawData();
				output.setInvoke( this.appInstance.getOriginInvoke() );
				output.setRawDataAttributes( attrs );
				
				this.appInstance.clearOrigin();
				this.appInstance.clearOriginInvoke();
				
				this.appInstance.updateLifeSpan();
				
				this.ec02Instance.setTimeout( 
						Long.toString( this.appInstance.getLifeSpan() )
						);
				
				this.nextState = States.ACTIVE;
				
			}
			
			
		/* NO FLOW : TIMEOUT ONLY */	
		} else {
			
			String noFlowStatistic = null;
			Alarm noFlowAlarm = null;
			
			EquinoxEvent e = EquinoxEvent.getEquinoxEventFrom( r.getRet() );
			
			SIPResponseCode noFlowResponseCode = null;
			
			switch ( e ) {
			case ERROR:
				noFlowStatistic				= null;
				noFlowAlarm					= null;
				noFlowResponseCode			= SIPResponseCode.SERVER_INTERNAL_ERROR;
				break;
				
			case REJECT:
				noFlowStatistic				= null;
				noFlowAlarm					= null;
				noFlowResponseCode			= SIPResponseCode.SERVER_INTERNAL_ERROR;
				break;
				
			case ABORT:
				noFlowStatistic				= null;
				noFlowAlarm					= null;
				noFlowResponseCode			= SIPResponseCode.SERVER_INTERNAL_ERROR;
				break;
				
			case TIMEOUT:
				noFlowStatistic				= null;
				noFlowAlarm					= null;
				noFlowResponseCode			= SIPResponseCode.SERVER_INTERNAL_ERROR;
				break;
				
			default:
				break;
			}
			
			// IO DETAIL - NO FLOW INPUT DETAIL
			InputDetail noFlowInputDetail = this.detail.new InputDetail(
					r.getInvoke(),
					new AppDetailEvent( "App", "Event" ),
					r.getType(),
					r.getRawDataAttributes(),
					r.getRawDataAttribute( "val" ),
					transactionStartTime - this.appInstance.getLastInstanceUpdatedTime()
					);
			
			noFlowInputDetail.setData( (ServerAssignmentMessage) JAXBHandler.createInstance(
					InstanceContext.getServerAssignmentContext(),
					r.getRawDataMessage(),
					ServerAssignmentMessage.class )
					);
			
			this.detail.getTransactionDetail().addInput( noFlowInputDetail );
			this.detail.getTransactionDetail().setInputTimeStamp( transactionStartTime );
			
			this.ec02Instance.incrementsStat( noFlowStatistic );
			this.ec02Instance.raiseAlarm( noFlowAlarm, new String[]{} );
			
			// GENERATE SIP MESSAGE
			SIPResponse noFlowResponse = new SIPResponse( noFlowResponseCode );
			noFlowResponse.setHeaders( this.appInstance.getSipMessage().getHeaders() );
			noFlowResponse.setContent( this.appInstance.getSipMessage().getContent() );
			
			HashMap<String, String> attrs = new HashMap<String, String>();
			attrs.put( "name", "SOCKET" );
			attrs.put( "ctype", "udp" );
			attrs.put( "type", MessageType.RESPONSE );
			attrs.put( "to", this.appInstance.getOrigin() );
			attrs.put( "val", noFlowResponse.toString() );
			
			EquinoxRawData output = new EquinoxRawData();
			output.setInvoke( this.appInstance.getOriginInvoke() );
			output.setRawDataAttributes( attrs );
			
			this.outputs.add( output );
			
			this.appInstance.clearOrigin();
			this.appInstance.clearOriginInvoke();
			
			this.nextState = States.ACTIVE;
			
			// UDR
			
			// IODETAL - NO FLOW OUTPUT DETAIL
			OutputDetail noFlowOutputDetail = this.detail.new OutputDetail(
					output.getInvoke(),
					new AppDetailEvent( "App", "Event" ),
					output.getType(),
					output.getRawDataAttributes(),
					output.getRawDataAttribute( "val" )
					);
			
			noFlowOutputDetail.setData( noFlowResponse );
			
			this.detail.getTransactionDetail().addOutput( noFlowOutputDetail );
		}
		
		/* */
		long transactionEndTime = System.currentTimeMillis();
		
		this.appInstance.setLastInstanceUpdatedTime( transactionEndTime );
		
		this.ec02Instance.setEquinoxRawDatas( this.outputs );
		this.ec02Instance.setRet( RetNumber.NORMAL );
		
		this.detail.getTransactionDetail().setOutputTimeStamp( transactionEndTime );
		
		this.detail.setNextState( this.nextState );
		this.detail.setProcessingTime( transactionEndTime - transactionStartTime );
		
		System.out.println( "---" );
		System.out.println(
				new GsonBuilder().setPrettyPrinting().disableHtmlEscaping().create().toJson( this.detail )
				);
		
		return this.nextState;
		
	}
}
			
