package th.co.ais.swrtc.states;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import th.co.ais.swrtc.enums.Alarm;
import th.co.ais.swrtc.enums.EquinoxEvent;
import th.co.ais.swrtc.enums.SIPRequestMethod;
import th.co.ais.swrtc.enums.SIPResponseCode;
import th.co.ais.swrtc.exception.SIPParseException;
import th.co.ais.swrtc.exception.VerificationFailedException;
import th.co.ais.swrtc.instances.APPInstance;
import th.co.ais.swrtc.instances.EC02Instance;
import th.co.ais.swrtc.instances.SIPMessage;
import th.co.ais.swrtc.instances.SIPResponse;
import th.co.ais.swrtc.instances.ServerAssignmentMessage;
import th.co.ais.swrtc.interfaces.APPConfig;
import th.co.ais.swrtc.interfaces.CallTerminationMethod;
import th.co.ais.swrtc.interfaces.Event;
import th.co.ais.swrtc.interfaces.MessageType;
import th.co.ais.swrtc.interfaces.RetNumber;
import th.co.ais.swrtc.interfaces.States;
import th.co.ais.swrtc.jaxb.InstanceContext;
import th.co.ais.swrtc.jaxb.JAXBHandler;
import th.co.ais.swrtc.utils.AppDetail;
import th.co.ais.swrtc.utils.AppDetail.InputDetail;
import th.co.ais.swrtc.utils.AppDetail.OutputDetail;
import th.co.ais.swrtc.utils.AppDetailEvent;
import th.co.ais.swrtc.utils.ConfigureTool;
import th.co.ais.swrtc.utils.IDGenerator;
import th.co.ais.swrtc.utils.SIPParser;
import th.co.ais.swrtc.verificator.SIPVerificator;

import com.google.gson.GsonBuilder;

import ec02.af.abstracts.AbstractAF;
import ec02.af.data.EquinoxRawData;
import ec02.af.interfaces.IAFState;

public class INITIATE_TERMINATION implements IAFState {
	
	private ArrayList<EquinoxRawData> outputs;
	
	private EC02Instance ec02Instance;
	
	private APPInstance appInstance;
	private AppDetail detail;
	
	private String currentState;
	private String nextState;
	
	private SIPMessage sip;
	
	@Override
	public String doAction( AbstractAF af, Object instance, ArrayList<EquinoxRawData> rawDatas ) {
		
		long transactionStartTime = System.currentTimeMillis();
		
		this.ec02Instance = ( EC02Instance ) instance;
		this.appInstance = this.ec02Instance.getAppInstance();
		
		this.outputs = new ArrayList<EquinoxRawData>();
		
		this.currentState = this.ec02Instance.getProperties().getState();
		
		this.detail = new AppDetail();
		this.detail.setCurrentState( this.ec02Instance.getProperties().getState() );
		this.detail.getTransactionDetail().setSession( "equinox-session" );
		this.detail.getTransactionDetail().setInitialInvoke( "initial-invoke" );
		
		// FOR EACH RAW-DATA
		for ( EquinoxRawData r : rawDatas ) {
			
			String invoke = r.getInvoke();
			
			/* NORMAL FLOW */
			if ( RetNumber.NORMAL.equals( r.getRet() ) ) {
				
				if ( ! this.appInstance.getPendingRequests().contains( invoke ) ) {
					continue;
					
				}
				
				this.appInstance.completePendingRequest( invoke );
				
				String normalRawDataMessage = r.getRawDataCDATAAttributes( "val" );
				
				try {
					
					// IODETAIL
					InputDetail normalInputDetail = this.detail.new InputDetail(
							r.getInvoke(),
							new AppDetailEvent( r.getOrig(), "Test-Command" ),
							r.getType(),
							r.getRawDataAttributes(),
							normalRawDataMessage, null
							);
					
					SIPMessage sip = SIPParser.getMessage( normalRawDataMessage );
					
					normalInputDetail.setData( sip );
					
					this.detail.getTransactionDetail().addInput( normalInputDetail );
					this.detail.getTransactionDetail().setInputTimeStamp( transactionStartTime );
					
					// TERMINATION STARTED FROM AS
					if ( Event.WAIT_200_FROM_BOTH_SIDE.equals( this.appInstance.getEvent() ) ) {
						
						// VERIFY '200' MESSAGE
						SIPVerificator.verify( SIPResponseCode.OK, sip );
						
					}
					// TERMINATION STARTED FROM P OR S
					else {
						
						if ( Event.WAIT_BYE_FROM_AS.equals( this.appInstance.getEvent() ) ) {
							
							// VERIFY 'BYE' MESSAGE
							
							// FORWARD 'BYE'
							HashMap<String, String> attrs = new HashMap<String, String>();
							attrs.put( "name", "SOCKET" );
							attrs.put( "ctype", "udp" );
							attrs.put( "type", MessageType.REQUEST );
							attrs.put( "val", this.sip.toString() );
							
							if ( CallTerminationMethod.BYE_FROM_HERE.equals( this.appInstance.getCallTerminationMethod() ) ) {
								attrs.put( "to", ConfigureTool.getConfigure( APPConfig.TERM_SWRTC_INTERFACE ) );
								
							}
							// BYE FROM OPPOSITE SIDE
							else {
								attrs.put( "to", ConfigureTool.getConfigure( APPConfig.PWRTC_INTERFACE ) );
								
							}
							
							EquinoxRawData output = new EquinoxRawData();
							output.setInvoke( IDGenerator.generateUniqueId() );
							output.setRawDataAttributes( attrs );
							
							this.outputs.add( output );
							
							this.appInstance.addPendingRequest( output.getInvoke() );
							this.appInstance.setEvent( Event.WAIT_200_AFTER_BYE );
							
							this.ec02Instance.setTimeout( ConfigureTool.getConfigure( APPConfig.RESPONSE_TIME_LIMIT ) );
							
						}
						else if ( Event.WAIT_200_AFTER_BYE.equals( this.appInstance.getEvent() ) ) {
							
							// VERIFY '200' MESSAGE
							
							// FORWARD '200'
							HashMap<String, String> attrs = new HashMap<String, String>();
							attrs.put( "name", "SOCKET" );
							attrs.put( "ctype", "udp" );
							attrs.put( "type", MessageType.REQUEST );
							attrs.put( "val", this.sip.toString() );
							attrs.put( "to", ConfigureTool.getConfigure( APPConfig.AS_INTERFACE ) );
							
							EquinoxRawData output = new EquinoxRawData();
							output.setInvoke( IDGenerator.generateUniqueId() );
							output.setRawDataAttributes( attrs );
							
							this.outputs.add( output );
							
							this.appInstance.addPendingRequest( output.getInvoke() );
							this.appInstance.setEvent( Event.WAIT_200_FROM_AS );
							
							this.ec02Instance.setTimeout( ConfigureTool.getConfigure( APPConfig.RESPONSE_TIME_LIMIT ) );
							
						}
						else if ( Event.WAIT_200_FROM_AS.equals( this.appInstance.getEvent() ) ) {
							
							// VERIFY '200' MESSAGE
							
						}
						
					}
					
				} catch ( SIPParseException e ) {

					HashMap<String, String> attrs = new HashMap<String, String>();
					attrs.put( "name", "SOCKET" );
					attrs.put( "ctype", "udp" );
					attrs.put( "type", MessageType.RESPONSE );
					attrs.put( "to", this.appInstance.getOrigin() );
					attrs.put( "val", " SIP PARSE EXCEPTION " );
					
					EquinoxRawData output = new EquinoxRawData();
					output.setInvoke( this.appInstance.getOriginInvoke() );
					output.setRawDataAttributes( attrs );
					
					this.appInstance.clearOrigin();
					this.appInstance.clearOriginInvoke();
					
					this.appInstance.updateLifeSpan();
					
					this.ec02Instance.setTimeout( 
							Long.toString( this.appInstance.getLifeSpan() )
							);
					
					this.nextState = States.ACTIVE;
					
				} catch (VerificationFailedException e) {

					HashMap<String, String> attrs = new HashMap<String, String>();
					attrs.put( "name", "SOCKET" );
					attrs.put( "ctype", "udp" );
					attrs.put( "type", MessageType.RESPONSE );
					attrs.put( "to", this.appInstance.getOrigin() );
					attrs.put( "val", " INVITE VERIFICATION FAILED EXCEPTION " );
					
					EquinoxRawData output = new EquinoxRawData();
					output.setInvoke( this.appInstance.getOriginInvoke() );
					output.setRawDataAttributes( attrs );
					
					this.appInstance.clearOrigin();
					this.appInstance.clearOriginInvoke();
					
					this.appInstance.updateLifeSpan();
					
					this.ec02Instance.setTimeout( 
							Long.toString( this.appInstance.getLifeSpan() )
							);
					
					this.nextState = States.ACTIVE;
					e.printStackTrace();
					
				}
			
			} 
			/* NO FLOW */
			else {
				String noFlowStatistic = null;
				Alarm noFlowAlarm = null;
				
				EquinoxEvent e = EquinoxEvent.getEquinoxEventFrom( r.getRet() );
				
				SIPResponseCode noFlowResponseCode = null;
				
				switch ( e ) {
				case ERROR:
					noFlowStatistic				= null;
					noFlowAlarm					= null;
					noFlowResponseCode			= SIPResponseCode.SERVER_INTERNAL_ERROR;
					break;
					
				case REJECT:
					noFlowStatistic				= null;
					noFlowAlarm					= null;
					noFlowResponseCode			= SIPResponseCode.SERVER_INTERNAL_ERROR;
					break;
					
				case ABORT:
					noFlowStatistic				= null;
					noFlowAlarm					= null;
					noFlowResponseCode			= SIPResponseCode.SERVER_INTERNAL_ERROR;
					break;
					
				case TIMEOUT:
					noFlowStatistic				= null;
					noFlowAlarm					= null;
					noFlowResponseCode			= SIPResponseCode.SERVER_INTERNAL_ERROR;
					break;
					
				default:
					break;
				}
				
				// IO DETAIL - NO FLOW INPUT DETAIL
				InputDetail noFlowInputDetail = this.detail.new InputDetail(
						r.getInvoke(),
						new AppDetailEvent( "App", "Event" ),
						r.getType(),
						r.getRawDataAttributes(),
						r.getRawDataAttribute( "val" ),
						transactionStartTime - this.appInstance.getLastInstanceUpdatedTime()
						);
				
				noFlowInputDetail.setData( (ServerAssignmentMessage) JAXBHandler.createInstance(
						InstanceContext.getServerAssignmentContext(),
						r.getRawDataMessage(),
						ServerAssignmentMessage.class )
						);
				
				this.detail.getTransactionDetail().addInput( noFlowInputDetail );
				this.detail.getTransactionDetail().setInputTimeStamp( transactionStartTime );
				
				this.ec02Instance.incrementsStat( noFlowStatistic );
				this.ec02Instance.raiseAlarm( noFlowAlarm, new String[]{} );
				
				// GENERATE SIP MESSAGE
				SIPResponse noFlowResponse = new SIPResponse( noFlowResponseCode );
				noFlowResponse.setHeaders( this.appInstance.getSipMessage().getHeaders() );
				noFlowResponse.setContent( this.appInstance.getSipMessage().getContent() );
				
				HashMap<String, String> attrs = new HashMap<String, String>();
				attrs.put( "name", "SOCKET" );
				attrs.put( "ctype", "udp" );
				attrs.put( "type", MessageType.RESPONSE );
				attrs.put( "to", this.appInstance.getOrigin() );
				attrs.put( "val", noFlowResponse.toString() );
				
				EquinoxRawData output = new EquinoxRawData();
				output.setInvoke( this.appInstance.getOriginInvoke() );
				output.setRawDataAttributes( attrs );
				
				this.outputs.add( output );
				
				this.nextState = States.ACTIVE;
				
				// UDR
				
				// IODETAL - NO FLOW OUTPUT DETAIL
				OutputDetail noFlowOutputDetail = this.detail.new OutputDetail(
						output.getInvoke(),
						new AppDetailEvent( "App", "Event" ),
						output.getType(),
						output.getRawDataAttributes(),
						output.getRawDataAttribute( "val" )
						);
				
				noFlowOutputDetail.setData( noFlowResponse );
				
				this.detail.getTransactionDetail().addOutput( noFlowOutputDetail );
			}
			
		} // END FOR EACH RAW-DATA
		
		long now 				= System.currentTimeMillis();
		long usedTime			= now - this.appInstance.getLastInstanceUpdatedTime();
		long timeoutRemaining 	= TimeUnit.SECONDS.toMillis( this.appInstance.getOutgoingTimeout() ) - usedTime;
		
		// BYE COMPLETED
		if ( this.appInstance.getPendingRequests().isEmpty() ) {
			
			// '200' BACK TO ORIGIN
			HashMap<String, String> attrs = new HashMap<String, String>();
			attrs.put( "name", "SOCKET" );
			attrs.put( "ctype", "udp" );
			attrs.put( "type", MessageType.RESPONSE );
			attrs.put( "val", this.sip.toString() );
			attrs.put( "to", this.appInstance.getOrigin() );
			
			EquinoxRawData output = new EquinoxRawData();
			output.setInvoke( this.appInstance.getOriginInvoke() );
			output.setRawDataAttributes( attrs );
			
			this.outputs.add( output );
			
			this.appInstance.clearOrigin();
			this.appInstance.clearOriginInvoke();
			this.appInstance.clearEvent();
			this.appInstance.clearCallTerminationMethod();
			
			/*
			 * BACKE TO 'ACTIVE' STATE WITH REMAINING LIFESPAN
			 */
			this.ec02Instance.setTimeout( Long.toString( this.appInstance.getLifeSpan() ) );
			
			this.nextState = States.ACTIVE;
			
		}
		// PENDING BYE IS AVAIL.
		else {
			
			if ( CallTerminationMethod.BYE_FROM_AS.equals( this.appInstance.getCallTerminationMethod() ) ) {
				timeoutRemaining = TimeUnit.MILLISECONDS.toSeconds( timeoutRemaining );
				timeoutRemaining = timeoutRemaining <= 0 ? 1 : timeoutRemaining;
				
				this.ec02Instance.setTimeout( Long.toString( timeoutRemaining ) );
				
				this.appInstance.setOutgoingTimeout( Integer.parseInt( this.ec02Instance.getTimeout() ) );
				
			}
			
			this.nextState = this.currentState;
			
		}
		
		/* */
		long transactionEndTime = System.currentTimeMillis();
		
		this.appInstance.setLastInstanceUpdatedTime( transactionEndTime );
		
		this.ec02Instance.setEquinoxRawDatas( this.outputs );
		this.ec02Instance.setRet( RetNumber.NORMAL );
		
		this.detail.getTransactionDetail().setOutputTimeStamp( transactionEndTime );
		
		this.detail.setNextState( this.nextState );
		this.detail.setProcessingTime( transactionEndTime - transactionStartTime );
		
		System.out.println( "---" );
		System.out.println(
				new GsonBuilder().setPrettyPrinting().disableHtmlEscaping().create().toJson( this.detail )
				);
		
		return this.nextState;
	}

}
