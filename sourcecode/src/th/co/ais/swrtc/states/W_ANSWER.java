package th.co.ais.swrtc.states;

import java.util.ArrayList;
import java.util.HashMap;

import th.co.ais.swrtc.enums.Alarm;
import th.co.ais.swrtc.enums.EquinoxEvent;
import th.co.ais.swrtc.enums.SIPRequestMethod;
import th.co.ais.swrtc.enums.SIPResponseCode;
import th.co.ais.swrtc.exception.SIPParseException;
import th.co.ais.swrtc.exception.VerificationFailedException;
import th.co.ais.swrtc.instances.APPInstance;
import th.co.ais.swrtc.instances.EC02Instance;
import th.co.ais.swrtc.instances.SIPMessage;
import th.co.ais.swrtc.instances.SIPRequest;
import th.co.ais.swrtc.instances.SIPResponse;
import th.co.ais.swrtc.instances.ServerAssignmentMessage;
import th.co.ais.swrtc.interfaces.APPConfig;
import th.co.ais.swrtc.interfaces.ActionType;
import th.co.ais.swrtc.interfaces.Event;
import th.co.ais.swrtc.interfaces.MessageType;
import th.co.ais.swrtc.interfaces.RetNumber;
import th.co.ais.swrtc.interfaces.States;
import th.co.ais.swrtc.interfaces.SubStates;
import th.co.ais.swrtc.jaxb.InstanceContext;
import th.co.ais.swrtc.jaxb.JAXBHandler;
import th.co.ais.swrtc.utils.AppDetail;
import th.co.ais.swrtc.utils.AppDetail.InputDetail;
import th.co.ais.swrtc.utils.AppDetail.OutputDetail;
import th.co.ais.swrtc.utils.AppDetailEvent;
import th.co.ais.swrtc.utils.ConfigureTool;
import th.co.ais.swrtc.utils.SIPHeaderHandler;
import th.co.ais.swrtc.utils.IDGenerator;
import th.co.ais.swrtc.utils.SIPParser;
import th.co.ais.swrtc.verificator.SIPVerificator;

import com.google.gson.GsonBuilder;

import ec02.af.abstracts.AbstractAF;
import ec02.af.data.EquinoxRawData;
import ec02.af.interfaces.IAFState;

public class W_ANSWER implements IAFState {
	
	private ArrayList<EquinoxRawData> outputs;
	
	private EC02Instance ec02Instance;
	
	private APPInstance appInstance;
	private AppDetail detail;
	
	private String currentState;
	private String nextState;

	@Override
	public String doAction( AbstractAF af, Object instance, ArrayList<EquinoxRawData> rawDatas ) {
		
		long transactionStartTime = System.currentTimeMillis();
		
		this.ec02Instance = ( EC02Instance ) instance;
		this.appInstance = this.ec02Instance.getAppInstance();
		
		this.currentState = this.ec02Instance.getProperties().getState();
		
		this.outputs = new ArrayList<EquinoxRawData>();
		
		this.detail = new AppDetail();
		this.detail.setCurrentState( this.ec02Instance.getProperties().getState() );
		this.detail.getTransactionDetail().setSession( "equinox-session" );
		this.detail.getTransactionDetail().setInitialInvoke( "initial-invoke" );
		
		EquinoxRawData r = rawDatas.get( 0 );
		
		/* NORMAL FLOW */
		if ( RetNumber.NORMAL.equals( r.getRet() ) ) {
			
			String normalRawDataMessage = r.getRawDataCDATAAttributes( "val" );
			
			try {
			
				String ownRecRouteAddress = "iwrtc.address";
				String ownViaAddress = "SIP/2.0/UDP iwrtc";
				
				String ownRouteAddress = "answer.address";
				String nextRouteAddress = "answer200.as.address";
				
				// IODETAIL
				InputDetail normalInputDetail = this.detail.new InputDetail(
						r.getInvoke(),
						new AppDetailEvent( r.getOrig(), "Test-Command" ),
						r.getType(),
						r.getRawDataAttributes(),
						normalRawDataMessage, null
						);
				
				SIPMessage sip = SIPParser.getMessage( normalRawDataMessage );
				
				normalInputDetail.setData( sip );
				
				this.detail.getTransactionDetail().addInput( normalInputDetail );
				this.detail.getTransactionDetail().setInputTimeStamp( transactionStartTime );
				
				// SIP RESPONSE
				if ( sip instanceof SIPResponse ) {
					
					// VERIFY INCOMING '200' RESPONSE
					SIPVerificator.verify( SIPResponseCode.OK, sip );
					
					this.appInstance.setOrigin( r.getOrig() );
					this.appInstance.setOriginInvoke( r.getInvoke() );
					
					SIPHeaderHandler.addRoute( sip, ownRouteAddress, nextRouteAddress );
					SIPHeaderHandler.addRecordRoute( sip, ownRecRouteAddress );
					SIPHeaderHandler.addVia( sip, ownViaAddress );
					
					// CREATE '200'
					SIPResponse ok = new SIPResponse( SIPResponseCode.OK );
					ok.setHeaders( sip.getHeaders() );
					ok.setContent( sip.getContent() );
					
					HashMap<String, String> okAttrs = new HashMap<String, String>();
					okAttrs.put( "name", "SOCKET" );
					okAttrs.put( "ctype", "udp" );
					okAttrs.put( "type", MessageType.REQUEST );
					okAttrs.put( "val", ok.toString() );
					okAttrs.put( "to", ConfigureTool.getConfigure( APPConfig.AS_INTERFACE ) );
					
					EquinoxRawData okOutput = new EquinoxRawData();
					
					okOutput.setInvoke( IDGenerator.generateUniqueId() );
					okOutput.setRawDataAttributes( okAttrs );
					
					this.outputs.add( okOutput );
					
					this.appInstance.addPendingRequest( okOutput.getInvoke() );
					this.appInstance.setEvent( Event.WAIT_200_FROM_AS );
					
					this.nextState = States.INITIATE_COMMUNICATION;
			
				
				} 
				// SIP REQUEST
				else { 
					
					// VERIFY INCOMING 'PRACK' MESSAGE
					SIPVerificator.verify( SIPRequestMethod.PRACK, sip );
					
					this.appInstance.setOrigin( r.getOrig() );
					this.appInstance.setOriginInvoke( r.getInvoke() );
					
					// CREATE 'PRACK'
					SIPRequest prack = new SIPRequest( SIPRequestMethod.PRACK, "<abc@123.4.5.6>" );
					prack.setHeaders( sip.getHeaders() );
					prack.setContent( sip.getContent() );
					
					HashMap<String, String> prackAttrs = new HashMap<String, String>();
					prackAttrs.put( "name", "SOCKET" );
					prackAttrs.put( "ctype", "udp" );
					prackAttrs.put( "type", MessageType.REQUEST );
					prackAttrs.put( "val", prack.toString() );
					prackAttrs.put( "to", ConfigureTool.getConfigure( APPConfig.TERM_SWRTC_INTERFACE ) );
					
					EquinoxRawData prackOutput = new EquinoxRawData();
					prackOutput.setInvoke( IDGenerator.generateUniqueId() );
					prackOutput.setRawDataAttributes( prackAttrs );
					
					this.outputs.add( prackOutput );
					
					this.appInstance.setSubState( SubStates.WAIT_PRACK_200 );
					this.appInstance.addPendingRequest( prackOutput.getInvoke() );
					
					this.nextState = this.currentState;
					
				}
				
			} catch ( SIPParseException e ) {

				HashMap<String, String> attrs = new HashMap<String, String>();
				attrs.put( "name", "SOCKET" );
				attrs.put( "ctype", "udp" );
				attrs.put( "type", MessageType.RESPONSE );
				attrs.put( "to", this.appInstance.getOrigin() );
				attrs.put( "val", " SIP PARSE EXCEPTION " );
				
				EquinoxRawData output = new EquinoxRawData();
				output.setInvoke( this.appInstance.getOriginInvoke() );
				output.setRawDataAttributes( attrs );
				
				this.appInstance.clearOrigin();
				this.appInstance.clearOriginInvoke();
				
				this.appInstance.updateLifeSpan();
				
				this.ec02Instance.setTimeout( 
						Long.toString( this.appInstance.getLifeSpan() )
						);
				
				this.nextState = States.ACTIVE;
				
			} catch (VerificationFailedException e) {

				HashMap<String, String> attrs = new HashMap<String, String>();
				attrs.put( "name", "SOCKET" );
				attrs.put( "ctype", "udp" );
				attrs.put( "type", MessageType.RESPONSE );
				attrs.put( "to", this.appInstance.getOrigin() );
				attrs.put( "val", " INVITE VERIFICATION FAILED EXCEPTION " );
				
				EquinoxRawData output = new EquinoxRawData();
				output.setInvoke( this.appInstance.getOriginInvoke() );
				output.setRawDataAttributes( attrs );
				
				this.appInstance.clearOrigin();
				this.appInstance.clearOriginInvoke();
				
				this.appInstance.updateLifeSpan();
				
				this.ec02Instance.setTimeout( 
						Long.toString( this.appInstance.getLifeSpan() )
						);
				
				this.nextState = States.ACTIVE;
				e.printStackTrace();
				
			}
			
		}
		/* NO FLOW */
		else {
			String noFlowStatistic = null;
			Alarm noFlowAlarm = null;
			
			EquinoxEvent e = EquinoxEvent.getEquinoxEventFrom( r.getRet() );
			
			SIPResponseCode noFlowResponseCode = null;
			
			switch ( e ) {
			case ERROR:
				noFlowStatistic				= null;
				noFlowAlarm					= null;
				noFlowResponseCode			= SIPResponseCode.SERVER_INTERNAL_ERROR;
				break;
				
			case REJECT:
				noFlowStatistic				= null;
				noFlowAlarm					= null;
				noFlowResponseCode			= SIPResponseCode.SERVER_INTERNAL_ERROR;
				break;
				
			case ABORT:
				noFlowStatistic				= null;
				noFlowAlarm					= null;
				noFlowResponseCode			= SIPResponseCode.SERVER_INTERNAL_ERROR;
				break;
				
			case TIMEOUT:
				noFlowStatistic				= null;
				noFlowAlarm					= null;
				noFlowResponseCode			= SIPResponseCode.SERVER_INTERNAL_ERROR;
				break;
				
			default:
				break;
			}
			
			// IO DETAIL - NO FLOW INPUT DETAIL
			InputDetail noFlowInputDetail = this.detail.new InputDetail(
					r.getInvoke(),
					new AppDetailEvent( "App", "Event" ),
					r.getType(),
					r.getRawDataAttributes(),
					r.getRawDataAttribute( "val" ),
					transactionStartTime - this.appInstance.getLastInstanceUpdatedTime()
					);
			
			noFlowInputDetail.setData( (ServerAssignmentMessage) JAXBHandler.createInstance(
					InstanceContext.getServerAssignmentContext(),
					r.getRawDataMessage(),
					ServerAssignmentMessage.class )
					);
			
			this.detail.getTransactionDetail().addInput( noFlowInputDetail );
			this.detail.getTransactionDetail().setInputTimeStamp( transactionStartTime );
			
			this.ec02Instance.incrementsStat( noFlowStatistic );
			this.ec02Instance.raiseAlarm( noFlowAlarm, new String[]{} );
			
			// GENERATE SIP MESSAGE
			SIPResponse noFlowResponse = new SIPResponse( noFlowResponseCode );
			noFlowResponse.setHeaders( this.appInstance.getSipMessage().getHeaders() );
			noFlowResponse.setContent( this.appInstance.getSipMessage().getContent() );
			
			HashMap<String, String> attrs = new HashMap<String, String>();
			attrs.put( "name", "SOCKET" );
			attrs.put( "ctype", "udp" );
			attrs.put( "type", MessageType.RESPONSE );
			attrs.put( "to", this.appInstance.getOrigin() );
			attrs.put( "val", noFlowResponse.toString() );
			
			EquinoxRawData output = new EquinoxRawData();
			output.setInvoke( this.appInstance.getOriginInvoke() );
			output.setRawDataAttributes( attrs );
			
			this.outputs.add( output );
			
			this.appInstance.clearOrigin();
			this.appInstance.clearOriginInvoke();
			
			this.nextState = States.ACTIVE;
			
			// UDR
			
			// IODETAL - NO FLOW OUTPUT DETAIL
			OutputDetail noFlowOutputDetail = this.detail.new OutputDetail(
					output.getInvoke(),
					new AppDetailEvent( "App", "Event" ),
					output.getType(),
					output.getRawDataAttributes(),
					output.getRawDataAttribute( "val" )
					);
			
			noFlowOutputDetail.setData( noFlowResponse );
			
			this.detail.getTransactionDetail().addOutput( noFlowOutputDetail );
		}
		
		/* */
		long transactionEndTime = System.currentTimeMillis();
		
		this.appInstance.setLastInstanceUpdatedTime( transactionEndTime );
		
		this.ec02Instance.setEquinoxRawDatas( this.outputs );
		this.ec02Instance.setRet( RetNumber.NORMAL );
		
		this.detail.getTransactionDetail().setOutputTimeStamp( transactionEndTime );
		
		this.detail.setNextState( this.nextState );
		this.detail.setProcessingTime( transactionEndTime - transactionStartTime );
		
		System.out.println( "---" );
		System.out.println(
				new GsonBuilder().setPrettyPrinting().disableHtmlEscaping().create().toJson( this.detail )
				);
		
		return this.nextState;
	}

}
