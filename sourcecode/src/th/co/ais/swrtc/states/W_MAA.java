package th.co.ais.swrtc.states;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import th.co.ais.swrtc.enums.Alarm;
import th.co.ais.swrtc.enums.EquinoxEvent;
import th.co.ais.swrtc.enums.SIPResponseCode;
import th.co.ais.swrtc.exception.VerificationFailedException;
import th.co.ais.swrtc.exception._ResultNotSuccess;
import th.co.ais.swrtc.instances.APPInstance;
import th.co.ais.swrtc.instances.EC02Instance;
import th.co.ais.swrtc.instances.MultimediaAuthMessage;
import th.co.ais.swrtc.instances.SIPResponse;
import th.co.ais.swrtc.interfaces.APPConfig;
import th.co.ais.swrtc.interfaces.MessageType;
import th.co.ais.swrtc.interfaces.RetNumber;
import th.co.ais.swrtc.interfaces.States;
import th.co.ais.swrtc.interfaces.Statistic;
import th.co.ais.swrtc.jaxb.InstanceContext;
import th.co.ais.swrtc.jaxb.JAXBHandler;
import th.co.ais.swrtc.utils.AppDetail;
import th.co.ais.swrtc.utils.AppDetail.InputDetail;
import th.co.ais.swrtc.utils.AppDetail.OutputDetail;
import th.co.ais.swrtc.utils.AppDetailEvent;
import th.co.ais.swrtc.utils.ConfigureTool;
import th.co.ais.swrtc.utils.MD5;
import th.co.ais.swrtc.utils.SIPHeaderFields;
import th.co.ais.swrtc.verificator.MultimediaAuthVerificator;

import com.google.gson.GsonBuilder;

import ec02.af.abstracts.AbstractAF;
import ec02.af.data.EquinoxRawData;
import ec02.af.interfaces.IAFState;

public class W_MAA implements IAFState {
	
	private ArrayList<EquinoxRawData> outputs;
	
	private EC02Instance ec02Instance;
	
	private APPInstance appInstance;
	private AppDetail detail;
	
	private String currentState;
	private String nextState;

	@Override
	public String doAction( AbstractAF af, Object instance, ArrayList<EquinoxRawData> rawDatas ) {
		
		long transactionStartTime = System.currentTimeMillis();
		
		this.ec02Instance = ( EC02Instance ) instance;
		this.appInstance = this.ec02Instance.getAppInstance();
		
		this.currentState = this.ec02Instance.getProperties().getState();
		
		this.outputs = new ArrayList<EquinoxRawData>();
		
		this.detail = new AppDetail();
		this.detail.setCurrentState( this.ec02Instance.getProperties().getState() );
		this.detail.getTransactionDetail().setSession( "equinox-session" );
		this.detail.getTransactionDetail().setInitialInvoke( "initial-invoke" );
		
		EquinoxRawData r = rawDatas.get( 0 );
		
		String pendingRequest = this.appInstance.getPendingRequests().get( 0 ); 
		
		/* NORMAL FLOW */
		if ( RetNumber.NORMAL.equals( r.getRet() ) ) {
			
			if ( ! r.getInvoke().equals( pendingRequest ) ) {
				return currentState;
			}
			
			this.appInstance.completePendingRequest( pendingRequest );
			
			String normalRawDataMessage = r.getRawDataMessage();
			
			// IO DETAIL - INCOMING MULTIMEDIA-AUTHEN-ANSWER
			InputDetail normalInputDetail = this.detail.new InputDetail(
					r.getInvoke(),
					new AppDetailEvent( r.getOrig(), "Receive-MAA-Back-From-mAAA" ),
					r.getType(),
					r.getRawDataAttributes(),
					normalRawDataMessage, 
					transactionStartTime - this.appInstance.getLastInstanceUpdatedTime()
					);
			
			String maaResponse = "<MultimediaAuthen>" + r.getRawDataMessage() + "</MultimediaAuthen>"; 
			
			MultimediaAuthMessage maa = ( MultimediaAuthMessage ) JAXBHandler.createInstance( 
					InstanceContext.getMultimediaAuthContext(), 
					maaResponse, 
					MultimediaAuthMessage.class 
					);
			
			normalInputDetail.setData( maa );
			
			this.detail.getTransactionDetail().addInput( normalInputDetail );
			this.detail.getTransactionDetail().setInputTimeStamp( transactionStartTime );
			
			try {
				
				// VERIFY 'MAA' MESSAGE
				MultimediaAuthVerificator.verifyMultimediaAuthAnswer( maa );
				
				/* GENERATE NONCE
				 * 
				 * 	DIGEST REALM - RESPONSE DATA FIELD 'REALM' FROM MAA
				 * 	NONCE - RANDOM NUMBER THAT ENCODE WITH BASE64
				 * 
				 */
				String nonce = MD5.encrypt( "ABCDEFGHI" );
				
				this.appInstance.setGeneratedNonce( nonce );
				
				// GENERATE '401' MESSAGE
				SIPResponse resp401 = new SIPResponse( SIPResponseCode.UNAUTHORIZED );
				resp401.setHeaders( this.appInstance.getSipMessage().getHeaders() );
				resp401.setContent( this.appInstance.getSipMessage().getContent() );
				
				String authenticationVector = 
						"Digest realm=\"test.3gpp.com\", " + 
						"nonce=\"" + this.appInstance.getGeneratedNonce() + "\", " + 
						"qop=\"auth\", " + 
						"opaque=\"AUDIOSLAVEDidLdkwKSHDl7\", algorithm=\"AKAv1-MD5\"";
				
				resp401.getHeaders().put( 
						SIPHeaderFields.WWW_AUTHENTICATE, 
						new ArrayList<String>( Arrays.asList( authenticationVector ) ) 
						);
				
				// INITIATE RAW DATA ATTRIBUTES
				HashMap<String, String> attrs = new HashMap<String, String>();
				attrs.put( "name", "SOCKET" );
				attrs.put( "ctype", "udp" );
				attrs.put( "type", MessageType.RESPONSE );
				attrs.put( "to", this.appInstance.getOrigin() );
				attrs.put( "val", resp401.toString() );
				
				// INITIATE RAW DATA OUTPUT
				EquinoxRawData output = new EquinoxRawData();
				output.setInvoke( this.appInstance.getOriginInvoke() );
				output.setRawDataAttributes( attrs );

				this.outputs.add( output );
				
				this.appInstance.clearOrigin();
				this.appInstance.clearOriginInvoke();
				this.appInstance.clearSIPMessage();
				
				this.ec02Instance.setTimeout( 
						ConfigureTool.getConfigure( APPConfig.MAXIMUM_WAIT_TIME_FOR_2ND_REGIS ) 
						);
				
				this.nextState = States.WAIT_SECOND_REGISTER;
				
				// IODETAIL - OUTGOING '401' SIP RESPONSE
				OutputDetail resp401Detail = this.detail.new OutputDetail(
						output.getInvoke(),
						new AppDetailEvent( "sWRTC", "401-back-Orig-P" ),
						output.getType(),
						output.getRawDataAttributes(),
						output.getRawDataAttribute( "val" )
						);
				
				resp401Detail.setData( resp401 );
				
				this.detail.getTransactionDetail().addOutput( resp401Detail );
				
			}
			catch ( VerificationFailedException e ) {
				
				HashMap<String, String> attrs = new HashMap<String, String>();
				attrs.put( "name", "SOCKET" );
				attrs.put( "ctype", "udp" );
				attrs.put( "type", MessageType.RESPONSE );
				attrs.put( "to", this.appInstance.getOrigin() );
				attrs.put( "val", " MAA VERIFICATION FAILED EXCEPTION " );
				
				EquinoxRawData output = new EquinoxRawData();
				output.setInvoke( this.appInstance.getOriginInvoke() );
				output.setRawDataAttributes( attrs );
				
				this.appInstance.clearOrigin();
				this.appInstance.clearOriginInvoke();
				
				this.nextState = States.IDLE;
				
			} 
			catch ( _ResultNotSuccess e ) {
				
				HashMap<String, String> attrs = new HashMap<String, String>();
				attrs.put( "name", "SOCKET" );
				attrs.put( "ctype", "udp" );
				attrs.put( "type", MessageType.RESPONSE );
				attrs.put( "to", this.appInstance.getOrigin() );
				attrs.put( "val", " MAA RESULT-CODE IS NOT SUCCESS " );
				
				EquinoxRawData output = new EquinoxRawData();
				output.setInvoke( this.appInstance.getOriginInvoke() );
				output.setRawDataAttributes( attrs );
				
				this.appInstance.clearOrigin();
				this.appInstance.clearOriginInvoke();
				
				this.nextState = States.IDLE;
				
			}
			
		}
		/* NO FLOW */
		else {
			
			String noFlowStatistic = null;
			Alarm noFlowAlarm = null;
			
			EquinoxEvent e = EquinoxEvent.getEquinoxEventFrom( r.getRet() );
			
			SIPResponseCode noFlowResponseCode = null;
			
			switch ( e ) {
			case ERROR:
				noFlowStatistic				= Statistic.APP_RECEIVE_MAR_REQUEST_ERROR;
				noFlowAlarm					= null;
				noFlowResponseCode			= SIPResponseCode.SERVER_INTERNAL_ERROR;
				break;
				
			case REJECT:
				noFlowStatistic				= Statistic.APP_RECEIVE_MAR_REQUEST_REJECT;
				noFlowAlarm					= null;
				noFlowResponseCode			= SIPResponseCode.SERVER_INTERNAL_ERROR;
				break;
				
			case ABORT:
				noFlowStatistic				= Statistic.APP_RECEIVE_MAR_REQUEST_ABORT;
				noFlowAlarm					= null;
				noFlowResponseCode			= SIPResponseCode.SERVER_INTERNAL_ERROR;
				break;
				
			case TIMEOUT:
				noFlowStatistic				= Statistic.APP_RECEIVE_MAR_REQUEST_TIMEOUT;
				noFlowAlarm					= Alarm.APP_RECEIVE_MAR_REQUEST_TIMEOUT;
				noFlowResponseCode			= SIPResponseCode.SERVER_TIME_OUT;
				break;
				
			default:
				break;
			}
			
			// IO DETAIL - NO FLOW INPUT DETAIL
			InputDetail noFlowInputDetail = this.detail.new InputDetail(
					r.getInvoke(),
					new AppDetailEvent( "App", "Event" ),
					r.getType(),
					r.getRawDataAttributes(),
					r.getRawDataAttribute( "val" ),
					transactionStartTime - this.appInstance.getLastInstanceUpdatedTime()
					);
			
			noFlowInputDetail.setData( ( MultimediaAuthMessage ) JAXBHandler.createInstance ( 
					InstanceContext.getMultimediaAuthContext(),
					r.getRawDataMessage(),
					MultimediaAuthMessage.class ) 
					);
			
			this.detail.getTransactionDetail().addInput( noFlowInputDetail );
			this.detail.getTransactionDetail().setInputTimeStamp( transactionStartTime );
			
			this.ec02Instance.incrementsStat( noFlowStatistic );
			this.ec02Instance.raiseAlarm( noFlowAlarm, new String[]{} );
			
			// GENERATE SIP MESSAGE
			SIPResponse noFlowResponse = new SIPResponse( noFlowResponseCode );
			noFlowResponse.setHeaders( this.appInstance.getSipMessage().getHeaders() );
			noFlowResponse.setContent( this.appInstance.getSipMessage().getContent() );
			
			HashMap<String, String> attrs = new HashMap<String, String>();
			attrs.put( "name", "SOCKET" );
			attrs.put( "ctype", "udp" );
			attrs.put( "type", MessageType.RESPONSE );
			attrs.put( "to", this.appInstance.getOrigin() );
			attrs.put( "val", noFlowResponse.toString() );
			
			EquinoxRawData output = new EquinoxRawData();
			output.setInvoke( this.appInstance.getOriginInvoke() );
			output.setRawDataAttributes( attrs );
			
			this.outputs.add( output );
			
			this.appInstance.clearOrigin();
			this.appInstance.clearOriginInvoke();
			
			this.nextState = States.IDLE;
			
			// UDR
			
			// IODETAL - NO FLOW OUTPUT DETAIL
			OutputDetail noFlowOutputDetail = this.detail.new OutputDetail(
					output.getInvoke(),
					new AppDetailEvent( "App", "Event" ),
					output.getType(),
					output.getRawDataAttributes(),
					output.getRawDataAttribute( "val" )
					);
			
			noFlowOutputDetail.setData( noFlowResponse );
			
			this.detail.getTransactionDetail().addOutput( noFlowOutputDetail );
			
		}
		
		/* */
		long transactionEndTime = System.currentTimeMillis();
		
		this.appInstance.setLastInstanceUpdatedTime( transactionEndTime );
		
		this.ec02Instance.setEquinoxRawDatas( this.outputs );
		this.ec02Instance.setRet( RetNumber.NORMAL );
		
		this.detail.getTransactionDetail().setOutputTimeStamp( transactionEndTime );
		
		this.detail.setNextState( this.nextState );
		this.detail.setProcessingTime( transactionEndTime - transactionStartTime );
		
		System.out.println( "---" );
		System.out.println(
				new GsonBuilder().setPrettyPrinting().disableHtmlEscaping().create().toJson( this.detail )
				);
		
		return this.nextState;
	}

}
