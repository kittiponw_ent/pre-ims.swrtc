package th.co.ais.swrtc.tester;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.sax.SAXSource;
import javax.xml.transform.sax.SAXTransformerFactory;
import javax.xml.transform.stream.StreamResult;

import org.xml.sax.InputSource;

import ec02.server.EC02Handler;
import ec02.server.EC02Server;

public class APPTester {
	
	public static final String examplePath = "./example/";
	public static final String tam = 
			"register/"
//			"s-origin/"
//			"s-endpoint/"
//			""
			;

	public APPTester() {
		
		String exampleFile = examplePath + tam +
				
//				"debugMsg.xml"
				
				/* REGISTER METHOD */
				
				"regis_idle.xml"
//				"regis_w_maa.xml"
//				"regis_2nd_register.xml"
//				"regis_w_saa.xml"
				
				/* CALL HANDLING METHOD */
				
//				"active_invite.xml"
//				"w_mnpdb.ldap.xml"
//				"init_invite.xml"
//				"init_invite_100.xml"
//				"init_invite_183.xml"
				
//				"w_ringing_receive_prack.xml"
//				"w_ringing_receive_prack_200.xml"
//				"w_ringing_receive_update.xml"
//				"w_ringing_receive_update_200.xml"
				
//				"w_ringing_receive_180.xml"
				
//				"w_answer_receive_prack.xml"
//				"w_answer_receive_prack_200.xml"
				
//				"w_answer_receive_200.xml"
				
//				"init_communication_200_from_AS.xml"
//				"init_communication_ack_from_P.xml"
//				"init_communication_ack_from_AS.xml"
				
//				"established_timeout.xml"
//				"established_bye_from_Here.xml"
//				"established_bye_from_I.xml"
//				"established_bye_from_AS.xml"
				
//				"init_termination_bye_from_AS_.xml"
//				"init_termination_bye_from_AS_AllAtOnce.xml"
				
//				"init_termination_caller_P_receive_bye_from_AS.xml"
//				"init_termination_caller_P_receive_200_from_S.xml"
//				"init_termination_caller_P_receive_200_from_AS.xml"
				
				;
		String str;
		String conf = "";
		String reqMessage = "";
		
		BufferedReader in = null;
		try {
			
			/** Input **/
			in = new BufferedReader( new FileReader( exampleFile ) );
			while ( ( str = in.readLine() ) != null) {
				reqMessage += (str + "\n");
			}
			in.close();
			
			/** EC02 Configuration **/
			in = new BufferedReader(new FileReader("./conf/APPConfiguration.xml"));
			while ( ( str = in.readLine() ) != null) {
				conf += str;
			}
			in.close();
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		String[] args = { "SMF", "SMF", "0", conf };
		try {
			EC02Server.main(args);
			
		} catch (IOException e) {
			e.printStackTrace();
			
		}
		
		EC02Handler handler = new EC02Handler();
		handler.verifyAFConfig( conf );
		
		String handlerMessage = handler.handle(reqMessage, 100000);
	
		/** RESULT **/
//		System.out.println("\n\n!-- INPUT --!\n");
//		System.out.println( reqMessage );
//		System.out.println( StringEscapeUtils.escapeJava( reqMessage ) );
//		
		System.out.println("\n\n!-- OUTPUT --!\n");
		System.out.println( formatXml( handlerMessage ) );
		
		/** **/
		System.exit( 0 );
		
		
	}

	public static void main(String[] args) {
		
		new APPTester();

	}
	
	private String formatXml(String xml) {
        try {
            Transformer serializer = SAXTransformerFactory.newInstance().newTransformer();
            serializer.setOutputProperty(OutputKeys.INDENT, "yes");
            serializer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
            Source xmlSource = new SAXSource(new InputSource(new ByteArrayInputStream(xml.getBytes())));
            StreamResult res = new StreamResult(new ByteArrayOutputStream());
            serializer.transform(xmlSource, res);
            
            return new String(((ByteArrayOutputStream) res.getOutputStream()).toByteArray());
        	
        } catch (Exception e) {
            return xml;
        }
    }

}
