package th.co.ais.swrtc.utils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Map;

import org.apache.commons.lang.StringEscapeUtils;

import com.google.gson.annotations.SerializedName;

public class AppDetail {

	@SerializedName( "_ApplicationName.Details" )
	private TransactionDetail transactionDetail;
	
	@SerializedName( "CurrentState" )
	private String currentState;
	
	@SerializedName( "NextState" )
	private String nextState;
	
	@SerializedName( "ProcessingTime" )
	private long processingTime;
	
	public AppDetail() {
		this.setTransactionDetail( new TransactionDetail() );
	}
	
	public TransactionDetail getTransactionDetail() {
		return transactionDetail;
	}

	public void setTransactionDetail( TransactionDetail transactionDetail ) {
		this.transactionDetail = transactionDetail;
	}

	public String getCurrentState() {
		return currentState;
	}

	public void setCurrentState(String currentState) {
		this.currentState = currentState;
	}

	public long getProcessingTime() {
		return processingTime;
	}

	public void setProcessingTime(long processingTime) {
		this.processingTime = processingTime;
	}

	public String getNextState() {
		return nextState;
	}

	public void setNextState(String nextState) {
		this.nextState = nextState;
	}
	
	/*
	 * 
	 */
	public class TransactionDetail {
		
		@SerializedName( "Session" )
		private String session;
		
		@SerializedName( "InitialInvoke" )
		private String initialInvoke;
		
		@SerializedName( "Scenario" )
		private String scenario;
		
		@SerializedName( "MSISDN" )
		private String msisdn;
		
		@SerializedName( "Input" )
		private ArrayList< InputDetail > inputs;
		
		@SerializedName( "InputTimeStamp" )
		private String inputTimeStamp;
		
		@SerializedName( "Output" )
		private ArrayList< OutputDetail > outputs;
		
		@SerializedName( "OutputTimeStamp" )
		private String outputTimeStamp;
		
		private transient String timeStampFormat = "yyyymmdd hh:mm:ss.SSS";
		
		public String getSession() {
			return session;
		}
		
		public void setSession(String session) {
			this.session = session;
		}

		public String getInitialInvoke() {
			return initialInvoke;
		}

		public void setInitialInvoke(String initialInvoke) {
			this.initialInvoke = initialInvoke;
		}

		public String getScenario() {
			return scenario;
		}

		public void setScenario(String scenario) {
			this.scenario = scenario;
		}

		public ArrayList< InputDetail > getInput() {
			return inputs;
		}

		public void setInput(ArrayList< InputDetail > input) {
			this.inputs = input;
		}
		
		public void addInput( InputDetail input ) {
			if ( this.inputs == null ) {
				this.inputs = new ArrayList<AppDetail.InputDetail>();
			}
			
			this.inputs.add( input );
		}

		public ArrayList< OutputDetail > getOutput() {
			return outputs;
		}

		public void setOutput(ArrayList< OutputDetail > output) {
			this.outputs = output;
		}
		
		public void addOutput( OutputDetail output ) {
			if ( this.outputs == null ) {
				this.outputs = new ArrayList<AppDetail.OutputDetail>();
			}
			
			this.outputs.add( output );
		}

		public String getMsisdn() {
			return msisdn;
		}

		public void setMsisdn(String msisdn) {
			this.msisdn = msisdn;
		}

		public String getInputTimeStamp() {
			return inputTimeStamp;
		}

		public void setInputTimeStamp( long inputTimeStamp ) {
			this.inputTimeStamp = new SimpleDateFormat( this.timeStampFormat ).format( inputTimeStamp );
		}

		public String getOutputTimeStamp() {
			return outputTimeStamp;
		}

		public void setOutputTimeStamp(long outputTimeStamp) {
			this.outputTimeStamp = new SimpleDateFormat( this.timeStampFormat ).format( outputTimeStamp );
		}

	}
	
	/*
	 * 
	 */
	private class IODetail {
		
		@SerializedName( "Invoke" )
		private String invoke;
		
		@SerializedName( "Event" )
		private String event;
		
		@SerializedName( "Type" )
		private String type;
		
		@SerializedName( "RawDataAttribute" )
		private Map<String, String> rawDataAttribute;
		
		@SerializedName( "RawDataMessage" )
		private String rawDataMessage;
		
		@SerializedName( "Data" )
		private Object data;
		
		public String getInvoke() {
			return this.invoke;
		}
		
		public void setInvoke(String invoke) {
			this.invoke = invoke;
		}
		
		public String getEvent() {
			return event;
		}
		
		public void setEvent(String event) {
			this.event = event;
		}
		
		public String getType() {
			return type;
		}
		
		public void setType(String type) {
			this.type = type;
		}
		
		public Map<String, String> getRawDataAttribute() {
			return rawDataAttribute;
		}

		public void setRawDataAttribute(Map<String, String> rawDataAttribute) {
			this.rawDataAttribute = rawDataAttribute;
		}
		
		public String getRawDataMessage() {
			return rawDataMessage;
		}

		public void setRawDataMessage(String rawDataMessage) {
			if ( rawDataMessage != null ) {
//				this.rawDataMessage = StringEscapeUtils.escapeHtml( rawDataMessage );
				this.rawDataMessage = StringEscapeUtils.unescapeHtml( rawDataMessage );
			}
		}

		public Object getData() {
			return data;
		}

		public void setData(Object data) {
			this.data = data;
		}

	}
	
	/*
	 * 
	 */
	public class InputDetail extends IODetail {
		
		@SerializedName( "ResponseTime" )
		private Long responseTime;
		
		public InputDetail() {}
		
		public InputDetail( String invoke, AppDetailEvent event, String type, Map<String, String> rawDataAttribute, String rawDataMessage, Long responseTime ) {
			this.setInvoke( invoke );
			this.setEvent( event.toString() );
			this.setType( type );
			this.setRawDataAttribute( rawDataAttribute );
			this.setRawDataMessage( rawDataMessage );
			this.setResponseTime( responseTime );
		}
		
		public Long getResponseTime() {
			return responseTime;
		}

		public void setResponseTime(Long responseTime) {
			this.responseTime = responseTime;
		}
		
	}
	
	/*
	 * 
	 */
	public class OutputDetail extends IODetail {
		
		public OutputDetail() {}
		
		public OutputDetail( String invoke, AppDetailEvent event, String type, Map<String, String> rawDataAttribute, String rawDataMessage ) {
			this.setInvoke( invoke );
			this.setEvent( event.toString() );
			this.setType( type );
			this.setRawDataAttribute( rawDataAttribute );
			this.setRawDataMessage( rawDataMessage );
		}
		
	}

}
