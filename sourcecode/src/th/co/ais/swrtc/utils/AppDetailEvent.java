package th.co.ais.swrtc.utils;

public class AppDetailEvent {
	
	private String nodeName;
	private String scenario;
	
	private static final String delim = ".";
	
	public AppDetailEvent( String nodeName, String scenario ) {
		this.setNodeName( nodeName );
		this.setScenario( scenario );
	}
	
	public String getNodeName() {
		return nodeName;
	}
	
	public void setNodeName(String nodeName) {
		this.nodeName = nodeName;
	}

	public String getScenario() {
		return scenario;
	}

	public void setScenario(String scenario) {
		this.scenario = scenario;
	}

	@Override
	public String toString() {
		
		StringBuilder e = new StringBuilder();
		e.append( this.getNodeName() + delim );
		e.append( this.getScenario() );
		
		return e.toString();
	}

}
