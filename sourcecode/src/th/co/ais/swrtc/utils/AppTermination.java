package th.co.ais.swrtc.utils;

import th.co.ais.swrtc.interfaces.TerminationCode;


public class AppTermination {
	
	private String idleState 				= TerminationCode.SUCCESS;
	private String secondRegisterState 		= TerminationCode.SUCCESS;
	
	// IDLE
	public String getTerminationOfIdleState() {
		return idleState;
	}
	public void setTerminationOfIdleState( String code ) {
		this.idleState = code;
	}
	
	// WAIT_MULTIMEDIA_AUTH_ANSWER
	
	// WAIT_SECOND_REGISTER
	public String getTerminationOfWaitSecondRegisterState () {
		return secondRegisterState;
	}
	public void setTerminationOfWaitSecondRegisterState ( String code ) {
		this.secondRegisterState = code;
	}
	
	@Override
	public String toString() {
		
		StringBuilder term = new StringBuilder();
		
		term.append( this.getTerminationOfIdleState() );
		
		return term.toString();
	}
	
}
