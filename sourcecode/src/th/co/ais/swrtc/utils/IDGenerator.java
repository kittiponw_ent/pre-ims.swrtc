package th.co.ais.swrtc.utils;

import java.util.UUID;

public class IDGenerator {

	public static String generateUniqueId () {
		return UUID.randomUUID().toString().split( "-" )[0];
		
	}
	
	public static String generateUniqueIdWithPrefix ( String prefix ) {
		return prefix + "_" + UUID.randomUUID().toString().split( "-" )[0];
		
	}
	
}
