package th.co.ais.swrtc.utils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.zip.DataFormatException;

import th.co.ais.swrtc.instances.APPInstance;

import com.google.gson.Gson;
import com.sun.org.apache.xerces.internal.impl.dv.util.Base64;

public class InstanceHandler {

	// ENCODING
	public static String encode ( APPInstance instance ) {
		
		Gson gson = new Gson();
		byte[] bytes = null;
		String encoded = null;
		
		try {
			bytes = Zip.compressBytes( gson.toJson( instance).getBytes() );
			encoded = Base64.encode( bytes );
			
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return encoded;
	}
	
	// DECODING
	public static APPInstance decode ( String encoded ) {
		
		Gson gson = new Gson();
		byte[] decoded = Base64.decode( encoded );
		
		try {
			decoded = Zip.extractBytes( decoded );
			
//			System.out.println( new String(decoded).substring( 222 ) );
			
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (DataFormatException e) {
			e.printStackTrace();
		}
		
		APPInstance appInstance = gson.fromJson( new String( decoded ), APPInstance.class );
		
		return appInstance;
	}
}
