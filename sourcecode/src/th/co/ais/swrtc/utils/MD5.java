package th.co.ais.swrtc.utils;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;


public class MD5 {
	
	private static MessageDigest digester;

	static {
		try {
			digester = MessageDigest.getInstance( "MD5" );
			
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			
		}
	}
	
	public static String encrypt( String str ) {
		
        if ( str == null ) {
            throw new IllegalArgumentException("String to encript cannot be null or zero length");
            
        }

        digester.update( str.getBytes() );
        
        byte[] hashBytes = digester.digest();
        
        StringBuffer sb = new StringBuffer();
        
		for (byte b : hashBytes) {
			sb.append( String.format( "%02x", b & 0xff ) );
			
		}
		
        return sb.toString();
        
    }
	
}
