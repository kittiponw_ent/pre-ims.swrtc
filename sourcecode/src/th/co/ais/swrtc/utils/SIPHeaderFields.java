package th.co.ais.swrtc.utils;

public interface SIPHeaderFields {
	
	String Accept 				= "Accept";
	String CALL_ID				= "Call-ID";
	String CONTACT				= "Contact";
	String CONTENT_LENGTH		= "Content-Length";
	String CSEQ					= "CSeq";
	String Expires				= "Expires";
	String FROM					= "From";
	String TO					= "To";
	String VIA					= "Via";
	String User_Agent			= "User-Agent";
	String MAX_FORWARDS			= "Max-Forwards";
	String WWW_AUTHENTICATE		= "WWW-Authenticate";
	String ROUTE				= "Route";
	String RECORD_ROUTE			= "Record-Route";
	
}
