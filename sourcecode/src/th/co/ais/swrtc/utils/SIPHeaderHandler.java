package th.co.ais.swrtc.utils;

import java.util.ArrayList;

import th.co.ais.swrtc.instances.SIPMessage;

public class SIPHeaderHandler {
	
	public static void addRoute ( SIPMessage sipMessage, String ownRouteAddress, String nextRouteAddress ) {
		
		ArrayList<String> routeList;

		routeList = sipMessage.getHeaders().get( SIPHeaderFields.ROUTE );
		routeList = ( routeList != null ) ? routeList : new ArrayList<String>();
		
		if ( ! ownRouteAddress.isEmpty() ) {
			routeList.add( 0, ownRouteAddress );
		} 
		
		if ( ! nextRouteAddress.isEmpty() ) {
			routeList.add( 0, nextRouteAddress );
		}
		
		sipMessage.getHeaders().put( SIPHeaderFields.ROUTE , routeList );
		
	}
	
	public static void removeRoute ( SIPMessage sipMessage ) {
		
		ArrayList<String> routeList;
		routeList = sipMessage.getHeaders().get( SIPHeaderFields.ROUTE );

		if ( ( routeList != null ) && ! routeList.isEmpty() ) {
			routeList.remove( 0 );
		}
		
	}
	
	
	public static void addRecordRoute ( SIPMessage sipMessage, String ownRecRouteAddress ) {
		
		ArrayList<String> recRouteList = new ArrayList<String>();
		
		recRouteList = sipMessage.getHeaders().get( SIPHeaderFields.RECORD_ROUTE );
		recRouteList = ( recRouteList != null ) ? recRouteList : new ArrayList<String>();
		
		recRouteList.add( 0 , ownRecRouteAddress );
		sipMessage.getHeaders().put( SIPHeaderFields.RECORD_ROUTE , recRouteList );
		
	}
	
	public static void removeRecRoute ( SIPMessage sipMessage ) {
		
		ArrayList<String> recRouteList = new ArrayList<String>();
		
		recRouteList = sipMessage.getHeaders().get( SIPHeaderFields.RECORD_ROUTE );
		
		recRouteList.remove( 0 );
		
		sipMessage.getHeaders().put( SIPHeaderFields.RECORD_ROUTE , recRouteList );
	}
	
	public static void addVia ( SIPMessage sipMessage, String ownViaAddress ) {
		
		ArrayList<String> vaiList = new ArrayList<String>();
		
		vaiList = sipMessage.getHeaders().get( SIPHeaderFields.VIA );
		vaiList.add( 0 , ownViaAddress );
		
		sipMessage.getHeaders().put( SIPHeaderFields.VIA , vaiList );
		
	}
	
	public static void removeVia ( SIPMessage sipMessage ) {
		
		ArrayList<String> viaList = new ArrayList<String>();
		viaList = sipMessage.getHeaders().get( SIPHeaderFields.VIA );
		viaList.remove( 0 );
		
		sipMessage.getHeaders().put( SIPHeaderFields.VIA , viaList );
	}

}
