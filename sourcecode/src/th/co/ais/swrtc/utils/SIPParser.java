package th.co.ais.swrtc.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import th.co.ais.swrtc.enums.SIPRequestMethod;
import th.co.ais.swrtc.exception.SIPParseException;
import th.co.ais.swrtc.instances.SIPMessage;
import th.co.ais.swrtc.instances.SIPRequest;
import th.co.ais.swrtc.instances.SIPResponse;

public class SIPParser {
	
	public static final String SIPSEPARATOR = ": ";
	
	public static SIPMessage getMessage ( String sipMessage ) throws SIPParseException {
		
		HashMap<String, ArrayList<String>> _headers = new HashMap<String, ArrayList<String>>();
		StringBuffer _content = new StringBuffer();
		
		SIPMessage msg = null;
		
		BufferedReader bufferReader = new BufferedReader( new StringReader( sipMessage ) );
		
		try {
			String firstLine = bufferReader.readLine().trim() + "\n";
			String firstFour = firstLine.substring( 0, 4 );
			
			int spacePos = firstLine.indexOf( " " );

			/*
			 * GET FIRST LINE WHICH WILL DISTINGUISH BETWEEN REQUEST OR RESPONSE,
			 * AND WHAT VERSION OF SIP IT REPRESENTS.
			 * 
			 */
			if ( SIPRequestMethod.getSipRequestMethod( firstLine.substring( 0, firstLine.indexOf( " " ) ) ) != null ) {
				
				int secondSpacePos = firstLine.indexOf( " ", spacePos + 1 );
				
				String method 	= firstLine.substring( 0, spacePos );
				String uri		= firstLine.substring( spacePos + 1, secondSpacePos );
				String version 	= firstLine.substring( secondSpacePos + 1, firstLine.indexOf( "\n" ) );
				
				msg = new SIPRequest( method, uri, version );
				
			}
			else if ( firstFour.equals( "SIP/" ) ) {
				
				int secondSpacePos = firstLine.indexOf( " ", spacePos + 1 );
				
				String version 	= firstLine.substring( 0, spacePos );
				String code		= firstLine.substring( spacePos + 1, secondSpacePos );
				String reason	= firstLine.substring( secondSpacePos + 1, firstLine.indexOf( "\n" ) );
				
				msg = new SIPResponse( version, code, reason );
				
			}
			else {
				throw new SIPParseException();
				
			}
			
			/*
			 * GET THE ENTRIE HEADER
			 */
			String line, identifier, value;
			int contentLength = 0;
			
			while ( true ) {

				line = bufferReader.readLine();
				
				if ( line == null || "\n".equals( line ) || line.length() == 0 ) {
					break;
				}
				
				line = line.trim();
				
				int separatorPos = line.indexOf( SIPSEPARATOR );
				
				if ( separatorPos == -1 ) {
					throw new SIPParseException();
				}
				
				identifier 	= line.substring( 0, separatorPos );
				value 		= line.substring( separatorPos + SIPSEPARATOR.length(), line.length() );
				
				if ( ! _headers.containsKey( identifier ) ) {
					
					if ( SIPHeaderFields.ROUTE.equals( identifier ) || SIPHeaderFields.RECORD_ROUTE.equals( identifier ) ) {
						_headers.put( identifier, new ArrayList<String>( Arrays.asList( value.split( "," ) ) ) );
						
					}
					else {
						_headers.put( identifier, new ArrayList<String>( Arrays.asList( value ) ) );
						
					}
					
				} else {
					_headers.get( identifier ).add( value );
					
				}
				
				if ( SIPHeaderFields.CONTENT_LENGTH.equals( identifier ) ) {
					String len = line.substring( separatorPos + SIPSEPARATOR.length(), line.length() ).trim();
                    contentLength = Integer.parseInt( len );
                    
				}
				
			}
			
			/*
			 * GET THE SIP CONTENT
			 */
			int contentRead = 0;
			
			while ( contentRead < contentLength ) {
				line = bufferReader.readLine().trim() + "\r\n";
				contentRead += line.length();
				
				_content.append( line );
			}
			
			msg.setHeaders( _headers );
			msg.setContent( _content );
			
		} catch (IOException e) {
			return null;
			
		}
		
		return msg;
	}

}
