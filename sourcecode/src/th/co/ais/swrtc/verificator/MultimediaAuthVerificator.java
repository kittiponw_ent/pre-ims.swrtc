package th.co.ais.swrtc.verificator;

import th.co.ais.swrtc.exception.VerificationFailedException;
import th.co.ais.swrtc.exception._ResultNotSuccess;
import th.co.ais.swrtc.instances.MultimediaAuthMessage;

public class MultimediaAuthVerificator {

	public static void verifyMultimediaAuthAnswer ( MultimediaAuthMessage maa ) 
			throws VerificationFailedException, _ResultNotSuccess {
		
		// Check Session-Id
		final String sessionId = maa.getSessionId();
		if ( sessionId.isEmpty() || sessionId == null ) {
			throw new VerificationFailedException();
		}
	
		// Check Vendor-Specific-Application-Id
		final String vendorSpecificApplicationId = maa.getVendorSpecificApplicationId();
		if ( vendorSpecificApplicationId.isEmpty() || vendorSpecificApplicationId == null ) {
			throw new VerificationFailedException();
		}
		
		// Check result code
		final String resultCode = maa.getResultCode();
		if ( ! "2001".equals( resultCode ) ) {
			throw new _ResultNotSuccess();
		}
		
		// Check Auth-Session-State
		final String authSessionState = maa.getAuthSessionState();
		if ( authSessionState.isEmpty() || authSessionState == null ) {
			throw new VerificationFailedException();
		}
		
		// Check Origin-Host
		final String originHost = maa.getOriginHost();
		if ( originHost.isEmpty() || originHost == null ) {
			throw new VerificationFailedException();
		}
		
		// Check Origin-Realm
		final String originRealm = maa.getOriginRealm();
		if ( originRealm.isEmpty() || originRealm == null ) {
			throw new VerificationFailedException();
		}
		
	}

}
