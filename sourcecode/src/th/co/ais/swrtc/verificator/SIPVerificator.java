package th.co.ais.swrtc.verificator;

import java.util.ArrayList;
import java.util.List;

import th.co.ais.swrtc.enums.SIPRequestMethod;
import th.co.ais.swrtc.enums.SIPResponseCode;
import th.co.ais.swrtc.exception.VerificationFailedException;
import th.co.ais.swrtc.instances.SIPMessage;
import th.co.ais.swrtc.instances.SIPRequest;
import th.co.ais.swrtc.instances.SIPResponse;
import th.co.ais.swrtc.utils.SIPHeaderFields;

public class SIPVerificator {
	
	/*
	 * VERIFY SIP REQEUST MESSAGE
	 */
	public static void verify( SIPRequestMethod requestMethod, SIPMessage sip ) 
			throws VerificationFailedException {

		if ( ! ( sip instanceof SIPRequest ) ) {
			throw new VerificationFailedException();
			
		}
		
		SIPRequest s = ( SIPRequest ) sip;
		
		/*
		 * 1ST INVITE METHOD
		 * 
		 * MANDATORY (M) - SIP METHOD, CALL-ID, CSEQ, FROM, TO, VIA
		 * OPTIONAL CONDITION (OC) - AUTHORIZATION, CONTENT-ENCODING, CONTENT-TYPE, CONTENT-LENGTH
		 * 
		 * THROW '400 - BAD REQUEST' IN CASE OF PARAM MISSING OR INVALID
		 * 
		 */
		
	}

	/*
	 * VERIFY SIP RESPONSE MESSAGE
	 */
	public static void verify ( SIPResponseCode responseCode, SIPMessage sip ) 
			throws VerificationFailedException {
		
		if ( ! ( sip instanceof SIPResponse ) ) { throw new VerificationFailedException(); }
		
		SIPResponse s = ( SIPResponse ) sip;
		
		if ( ! responseCode.equals( SIPResponseCode.getResponseCode( s.getCode() ) ) ) {
			throw new VerificationFailedException();
		}
		
	}
	
	public static void verify ( List<SIPResponseCode> responseCodes, SIPMessage sip ) 
			throws VerificationFailedException {
		
		if ( ! ( sip instanceof SIPResponse ) ) { throw new VerificationFailedException(); }
		
		SIPResponse s = ( SIPResponse ) sip;
		
		if ( ! responseCodes.contains( SIPResponseCode.getResponseCode( s.getCode() ) ) ) {
			throw new VerificationFailedException();
		}
		
		doVerifyVia	( s.getHeaders().get( SIPHeaderFields.VIA ) );
		doVerifyFrom ( s.getHeaders().get( SIPHeaderFields.FROM ) );
		doVerifyTo ( s.getHeaders().get( SIPHeaderFields.TO ) );
		
	}
	
	private static void doVerifyVia ( ArrayList<String> arr ) throws VerificationFailedException {
		
		if ( arr == null ) {
			throw new VerificationFailedException();
		}
		
	}
	
	private static void doVerifyFrom ( ArrayList<String> arr ) throws VerificationFailedException {
		
	}
	
	private static void doVerifyTo ( ArrayList<String> arr ) throws VerificationFailedException {
		
	}


}
