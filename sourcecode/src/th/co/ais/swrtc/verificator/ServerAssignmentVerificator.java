package th.co.ais.swrtc.verificator;

import th.co.ais.swrtc.exception.VerificationFailedException;
import th.co.ais.swrtc.exception._ResultNotSuccess;
import th.co.ais.swrtc.instances.ServerAssignmentMessage;

public class ServerAssignmentVerificator {

	public static void verifyServerAssignmentAnswer ( ServerAssignmentMessage saa ) 
			throws VerificationFailedException, _ResultNotSuccess {
		
		// Check Session-Id
		final String sessionId = saa.getSessionId();
		if ( sessionId.isEmpty() || sessionId == null ) {
			throw new VerificationFailedException();
		}
	
		// Check Vendor-Specific-Application-Id
		final String vendorSpecificApplicationId = saa.getVendorSpecificApplicationId();
		if ( vendorSpecificApplicationId.isEmpty() || vendorSpecificApplicationId == null ) {
			throw new VerificationFailedException();
		}
		
		// Check result code
		final String resultCode = saa.getResultCode();
		if ( ! "2001".equals( resultCode ) ) {
			throw new _ResultNotSuccess();
		}
		
		// Check Auth-Session-State
		final String authSessionState = saa.getAuthSessionState();
		if ( authSessionState.isEmpty() || authSessionState == null ) {
			throw new VerificationFailedException();
		}
		
		// Check Origin-Host
		final String originHost = saa.getOriginHost();
		if ( originHost.isEmpty() || originHost == null ) {
			throw new VerificationFailedException();
		}
		
		// Check Origin-Realm
		final String originRealm = saa.getOriginRealm();
		if ( originRealm.isEmpty() || originRealm == null ) {
			throw new VerificationFailedException();
		}
	
	}

}
